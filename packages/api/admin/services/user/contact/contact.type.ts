import { ObjectType, ID, Field } from 'type-graphql';

@ObjectType()
export default class AdminContact {
  @Field(type => ID)
  id: string;

  @Field()
  type: string;

  @Field()
  number: string;
}
