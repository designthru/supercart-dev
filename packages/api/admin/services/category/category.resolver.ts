import { Resolver, Query, Arg, ID, Mutation } from 'type-graphql';
import loadCategories from '../../data/category.data';
import AdminCategory from './category.type';
import AddCategoryInput from './category.input_type';
import search from '../../helpers/search';
@Resolver()
export default class CategoryResolver {
  private readonly categoriesCollection: AdminCategory[] = loadCategories();

  @Query(returns => [AdminCategory], { description: 'Get all the categories' })
  async adminCategories(
    @Arg('type', { nullable: true }) type?: string,
    @Arg('searchBy', { defaultValue: '' }) searchBy?: string
  ): Promise<AdminCategory[]> {
    let categories = this.categoriesCollection;

    if (type) {
      categories = await categories.filter(category => category.type === type);
    }
    return await search(categories, ['name'], searchBy);
  }

  @Query(returns => AdminCategory)
  async adminCategory(
    @Arg('id', type => ID) id: string
  ): Promise<AdminCategory | undefined> {
    return await this.categoriesCollection.find(category => category.id === id);
  }

  @Mutation(() => AdminCategory, { description: 'Create Category' })
  async adminCreateCategory(
    @Arg('category') category: AddCategoryInput
  ): Promise<AdminCategory> {
    console.log(category, 'category');

    return await category;
  }
}
