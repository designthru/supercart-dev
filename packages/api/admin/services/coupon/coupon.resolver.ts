import { Resolver, Mutation, Arg, Query } from 'type-graphql';
import loadCoupons from '../../data/coupon.data';
import AdminCoupon from './coupon.type';
import AddCouponInput from './coupon.input_type';
import search from '../../helpers/search';
@Resolver()
export default class AdminCouponResolver {
  private readonly couponsCollection: AdminCoupon[] = loadCoupons();

  @Query(returns => [AdminCoupon], { description: 'Get All Coupons' })
  async adminCoupons(
    @Arg('status', { nullable: true }) status?: string,
    @Arg('searchBy', { nullable: true }) searchBy?: string
  ): Promise<AdminCoupon[] | undefined> {
    let coupons = this.couponsCollection;
    if (status) {
      coupons = coupons.filter(coupon => coupon.status === status);
    }
    return await search(coupons, ['title', 'code'], searchBy);
  }

  @Mutation(returns => AdminCoupon)
  async adminCreateCoupon(
    @Arg('coupon') coupon: AddCouponInput
  ): Promise<AdminCoupon | undefined> {
    console.log(coupon, 'coupon');

    return await coupon;
  }
}
