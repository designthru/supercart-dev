import { Customer } from '../entities/Customer';
import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export default class SigninPayload {
    @Field(() => Customer)
    customer: Customer;

    @Field()
    token: string;
}