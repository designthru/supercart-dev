import { ObjectType, Field, ID } from "type-graphql";
import { prop as Property, getModelForClass, pre, prop } from "@typegoose/typegoose";
import * as bcrypt from 'bcrypt';
import Address from '../types/address.type';
import Card from '../types/card.type';
import Contact from '../types/contact.type';
import { __Type } from "graphql";
import { Ref } from "../types/types";

async function save(this: Customer) {
    this.password = await bcrypt.hash(this.password, 10);
}

@pre<Customer>("save", save)

@ObjectType({ description: "The Customers model" })
export class Customer {
    @Field(() => ID)
    _id: string;

    @Field()
    @Property()
    email: String;
    
    @Field()
    @Property({unique: false})
    name: String;

    @Field()
    @Property()
    password: String;

    @Field()
    @Property()
    image: String;

    @Field()
    @Property()
    totalOrder: Number;

    @Field()
    @Property()
    totalOrderAmount: Number;

    @Field()
    @Property()
    address: String;

    @Field()
    @Property()
    card: String;

    @Field()
    @Property()
    contact: String;

    @Field()
    @Property()
    role: String;

    @Field()
    @Property()
    emailVerificationToken: String;
    
    @Field()
    @Property()
    isEmailVerified: Boolean;

    @Field()
    @Property()
    isPhoneVerified: Boolean;

    @Field({nullable: true})
    @Property()
    customerId: String;

    @Field(() => Date)
    createdAt: Date;

    @Field(() => Date)
    updatedAt: Date;

}

export const CustomerModel = getModelForClass(Customer, {schemaOptions: {timestamps: true}});