import { ObjectType, Field } from 'type-graphql';
import OrderProduct from './orderProduct.type';
// import { OrderStatusEnum } from './orderStatusEnum';

@ObjectType()
export default class Order {
  @Field()
  id: number;

  @Field()
  userId: number;

  @Field(type => [OrderProduct])
  products: OrderProduct[];

  @Field(type => String)
  status: number;

  @Field(type => String)
  deliveryTime: string;

  @Field(type => String)
  amount: number;

  @Field(type => String)
  subtotal: number;

  @Field(type => String)
  discount: number;

  @Field(type => String)
  shoppingFee: number; // $10 fixed fee based on minimum order amount $75.

  @Field(type => String)
  deliveryFee: number; // $15 fixed fee based on minimum order amount $75. 
                      // plus $10 for orders that exceed 100 items or => $300 in values  

  @Field(type => String)
  deliveryAddress: string; // User address and geolocation data

  @Field(type => String)
  date: string;
}
