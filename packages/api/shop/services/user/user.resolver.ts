import { Resolver, Query, Arg, Int, Mutation } from 'type-graphql';
import * as jwt from "jsonwebtoken";
import { filterItems } from '../../helpers/filter';
import User from './user.type';
import { MongoClient, ObjectId } from 'mongodb'
import * as bcrypt from 'bcrypt';
import { Customer, CustomerModel } from '../../entities/Customer';
import Sms from './sms.type';
import loadUsers from './user.sample';
import Address from '../../types/address.type';
import SigninPayload from '../../types/signin.type'
//import AddressInput from '../inputs/Address.input'
import { add } from 'lodash';
import { NodeMailgun } from 'ts-mailgun';

@Resolver()
export class UserResolver {
  private readonly items: User[] = loadUsers();
  //resolver for user signup
  @Mutation(() => SigninPayload)
  async signUp(
      @Arg('username') username: string,
      @Arg('email') email: string,
      @Arg('password') password: string
  ): Promise<any> {
    const newToken = jwt.sign({ username, password }, '123456aaa!@#', {
      expiresIn: "1h"
    });
    // window.localStorage.setItem('userToken', newToken);
    let customer = await CustomerModel.create(
      {
        name: username,
        email: email,
        password: password,
        totalOrder: 0,
        totalOrderAmount: 0.0,
        image: '',
        address: [],
        contact: [],
        card: [],
        role: "customer",
        isPhoneVerified: false,
        isEmailVerified: false,
        emailVerificationToken: "",
        customerId: "",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    );
    let data = {
      customer: customer,
      token: newToken
    }
    return data;
  }

  @Mutation(() => SigninPayload)
  async logIn(
    @Arg('email') email: string,
    @Arg('password') password: string
  ): Promise<any> {
    let customer: any;
    try {

        customer = await CustomerModel.findOne({email: email});
        let name = customer.name;
        let passwordA = customer.password;
        if (customer) {
          
          const newToken = jwt.sign({ name, passwordA }, process.env.JWT_SECURITY_KEY || "12345678", {
            expiresIn: "1h"
          });
          let passwordMatched = await bcrypt.compare(password, customer.password.toString());
          if (!passwordMatched) {
            throw new Error('Password is incorrect')
          } else {
            return { customer: customer, token: newToken };
          }
          // return {customer:}
        }
        else {
          console.log("Heressss");
          throw new Error('No customer found');
        }
    } catch (err) {
      if (err.message === 'Password is incorrect') {
        throw new Error('Password is incorrect');
      } else if (err.message === 'No customer found') {
        throw new Error('No customer found');
      }
    }
  }

  //resolver to get all user list
  @Query(() => [Customer])
  async getAllUsers(
    @Arg('data') data: string
  ): Promise<any> {
    console.log("here..")
    const customers = await CustomerModel.find({}).exec();
    // console.log("users --->", users);
    
    return customers;
    // return await this.items[0];
  }

  @Query(() => User)
  async me(@Arg('id') id: string): Promise<User> {
    // as auth user. check from middleware.
    return await this.items[0];
  }

  @Mutation(() => User, { description: 'Update User' })
  async updateMe(@Arg('meInput') meInput: string): Promise<User> {
    console.log(meInput, 'meInput');
    return await this.items[0];
  }

  @Mutation(() => User, { description: 'Add or Edit Address' })
  async updateAddress(
    @Arg('addressInput') addressInput: string
  ): Promise<User> {
    console.log(addressInput, 'addressinput');
    return await this.items[0];
  }

  @Mutation(() => User, { description: 'Add or Edit Contact' })
  async updateContact(
    @Arg('contactInput') contactInput: string
  ): Promise<User> {
    console.log(contactInput, 'contactinput');
    return await this.items[0];
  }

  @Mutation(() => User, { description: 'Add Payment Card' })
  async addPaymentCard(@Arg('cardInput') cardInput: string): Promise<User> {
    console.log(cardInput, 'cardInput');
    return await this.items[0];
  }

  @Mutation(() => User, { description: 'Delete Payment Card' })
  async deletePaymentCard(@Arg('cardId') cardId: string): Promise<User> {
    console.log(cardId, 'card_id');
    return await this.items[0];
  }

  @Mutation(() => Sms, { description: 'Send code to phone number' })
  async sendSmsCode(
    @Arg('phoneNumber') phoneNumber: string,
    @Arg('code') code: string
  ): Promise<any> {
    const accountSid = process.env.TWILIO_SID || 'AC23bcafdd225ba13b6ebe84af2eafe0e8';
    const authToken = process.env.TWILIO_AUTH_TOKEN || '638015a28ba37f94b2356725dbac0459';
    const client = require('twilio')(accountSid, authToken);
    let data = await client.messages
      .create({
        body: 'Your Supercart verification code is:  ' + code + ". Don't share this code with anyone. Our employees will never ask for the code.",
        from: '+12029465279',
        to: phoneNumber
      });
    console.log(data);
    return data;
  }

  @Mutation(() => SigninPayload, {description: 'Sign up from checkout page'})
  async signupFromCheckout(
    @Arg('name') name: string,
    @Arg('email') email: string,
    @Arg('password') password: string,
    @Arg('address') address: string,
    @Arg('contact') contact: string,
    @Arg('card') card: string,
  ): Promise<any> {
    const newToken = jwt.sign({ name, password }, '123456aaa!@#', {
      expiresIn: "1h"
    });
    let customer = await CustomerModel.create(
      {
        name: name,
        email: email,
        password: password,
        totalOrder: 0,
        totalOrderAmount: 0.0,
        image: '',
        address: address,
        contact: contact,
        card: card,
        role: "customer",
        isPhoneVerified: false,
        isEmailVerified: false,
        emailVerificationToken: "",
        customerId: "",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    );
    let data = {
      customer: customer,
      token: newToken
    }
    return data;
  }

  @Mutation(() => Customer, {description: 'Send verificaiton email'})
  async sendVerificationEmail(
    @Arg('id') id: string,
    @Arg('token') token: string
  ): Promise<any> {
    let isUpdated = await CustomerModel.updateOne({_id: new ObjectId(id)}, {emailVerificationToken: token});
    let customer: any =  await CustomerModel.findById(id);
    const mailer = new NodeMailgun();
    mailer.apiKey = 'ec75f567afb1dcdd7e7c9548a26b16a2-9a235412-ff302aec'; // Set your API key
    mailer.domain = 'stellaracademic.com'; // Set the domain you registered earlier
    mailer.fromEmail = 'noreply@my-sample-app.com'; // Set your from email
    mailer.fromTitle = 'My Sample App'; // Set the name you would like to send from

    mailer.init();
    const content = `<h2>Welcome to supercart app! Please click below link to verify your account</h2><br>
                    http://localhost:3000/email-verify/${token}`;
    // Send an email to test@example.com
    mailer
      .send(customer.email, 'Hello!', content)
      .then((result) => console.log('Done', result))
      .catch((error) => console.error('Error: ', error));
    return customer;
    
  }

  @Mutation(() => SigninPayload, {description: 'Check Email Verification Token'})
  async checkEmailToken(
    @Arg('token') token: string
  ): Promise<any> {
    console.log(token);
    await CustomerModel.updateOne({emailVerificationToken: token}, {isEmailVerified: true});
    let customer: any = await CustomerModel.findOne({emailVerificationToken: token});
    const name = customer.name;
    const password = customer.password;
    const newToken = jwt.sign({ name, password }, '123456aaa!@#', {
      expiresIn: "1h"
    });
    return { customer: customer, token: newToken};
  }

  @Mutation(() => SigninPayload, {description: 'Set phoneVerification field for customer true'})
  async doPhoneVerify(
    @Arg('id') id: string
  ): Promise<any> {
    console.log(id);
    await CustomerModel.updateOne({_id: new ObjectId(id)}, {isPhoneVerified: true});
    let customer: any = CustomerModel.findById(id);
    const name = customer.name;
    const password = customer.password;
    const newToken = jwt.sign({ name, password }, '123456aaa!@#', {
      expiresIn: "1h"
    })
    return {
      customer: customer,
      token: newToken
    }
  }

  @Mutation(() => Customer, {description: 'Add address to customer profile'})
  async addAddress(
    @Arg('userId') userId: string,
    @Arg('addressId') addressId: string,
    @Arg('type') type: string,
    @Arg('name') name: string,
    @Arg('info') info: string
  ): Promise<any> {
    let customer: any = await CustomerModel.findById(new ObjectId(userId));
    let address = JSON.parse(customer.address);
    address.push({
      id: addressId,
      type: address.length === 0 ? 'primary' : 'secondary',
      name: name,
      info: info
    });
    customer.address = JSON.stringify(address);
    await CustomerModel.updateOne({_id: new ObjectId(userId)}, customer);
    console.log(addressId, type, name, info);
    return customer;
  }

  @Mutation(() => Customer, { description: 'Edit address to customer profile' })
  async editAddress(
    @Arg('userId') userId: string,
    @Arg('addressId') addressId: string,
    @Arg('type') type: string,
    @Arg('name') name: string,
    @Arg('info') info: string
  ): Promise<any> {
    let customer: any = await CustomerModel.findById(new ObjectId(userId));
    let address = JSON.parse(customer.address);
    address.forEach((ele: any) => {
      if (ele.id === addressId) {
        ele.name = name;
        ele.info = info;
      }
    });
    customer.address = address;
    customer.address = JSON.stringify(address);
    await CustomerModel.updateOne({ _id: new ObjectId(userId) }, customer);
    return customer;
  }

  @Mutation(() => Customer, { description: 'change primary address of customer' })
  async changePrimaryAddress(
    @Arg('userId') userId: string,
    @Arg('addressId') addressId: string
  ): Promise<any> {
    let customer: any = await CustomerModel.findById(new ObjectId(userId));
    let address = JSON.parse(customer.address);
    address.forEach((ele: any) => {
      ele.type = 'secondary';
      if (ele.id === addressId) {
        ele.type = 'primary';
      }
    });
    customer.address = address;
    customer.address = JSON.stringify(address);
    await CustomerModel.updateOne({ _id: new ObjectId(userId) }, customer);
    return customer;
  }

  @Mutation(() => Customer, { description: "delete customer's address" })
  async deleteAddress(
    @Arg('userId') userId: string,
    @Arg('addressId') addressId: string
  ): Promise<any> {
    let customer: any = await CustomerModel.findById(new ObjectId(userId));
    let address = JSON.parse(customer.address);
    let newAddress = address.filter((ele: any) => ele.id !== addressId);
    customer.address = JSON.stringify(newAddress);
    await CustomerModel.updateOne({ _id: new ObjectId(userId) }, customer);
    return customer;
  }

  @Mutation(() => Customer, { description: "add customer's contact" })
  async addContact(
    @Arg('userId') userId: string,
    @Arg('contactId') contactId: string,
    @Arg('type') type: string,
    @Arg('number') number: string
  ): Promise<any> {
    let customer: any = await CustomerModel.findById(new ObjectId(userId));
    let contact = JSON.parse(customer.contact);
    contact.push({
      id: contactId,
      type: contact.length === 0 ? 'primary' : 'secondary',
      number:number
    })
    customer.contact = JSON.stringify(contact);
    await CustomerModel.updateOne({ _id: new ObjectId(userId) }, customer);
    return customer;
  }

  @Mutation(() => Customer, { description: "edit customer's contact" })
  async editContact(
    @Arg('userId') userId: string,
    @Arg('contactId') contactId: string,
    @Arg('type') type: string,
    @Arg('number') number: string
  ): Promise<any> {
    let customer: any = await CustomerModel.findById(new ObjectId(userId));
    let contact = JSON.parse(customer.contact);
    contact.forEach((ele: any) => {
      if (ele.id === contactId) {
        ele.number = number;
      }
    });
    customer.contact = contact;
    customer.contact = JSON.stringify(contact);
    await CustomerModel.updateOne({ _id: new ObjectId(userId) }, customer);
    return customer;
  }

  @Mutation(() => Customer, { description: "delete customer's contact" })
  async deleteContact(
    @Arg('userId') userId: string,
    @Arg('contactId') contactId: string
  ): Promise<any> {
    let customer: any = await CustomerModel.findById(new ObjectId(userId));
    let contact = JSON.parse(customer.contact);
    let newContact = contact.filter((ele: any) => ele.id !== contactId);
    customer.contact = JSON.stringify(newContact);
    await CustomerModel.updateOne({ _id: new ObjectId(userId) }, customer);
    return customer;
  }

  @Mutation(() => Customer, { description: 'change primary contact of customer' })
  async changePrimaryContact(
    @Arg('userId') userId: string,
    @Arg('contactId') contactId: string
  ): Promise<any> {
    let customer: any = await CustomerModel.findById(new ObjectId(userId));
    let contact = JSON.parse(customer.contact);
    contact.forEach((ele: any) => {
      ele.type = 'secondary';
      if (ele.id === contactId) {
        ele.type = 'primary';
      }
    });
    customer.contact = contact;
    customer.contact = JSON.stringify(contact);
    await CustomerModel.updateOne({ _id: new ObjectId(userId) }, customer);
    return customer;
  }

  @Mutation(() => Customer, { description: "add customer's card" })
  async addCard(
    @Arg('userId') userId: string,
    @Arg('cardId') cardId: string,
    @Arg('type') type: string,
    @Arg('exp_month') exp_month: string,
    @Arg('exp_year') exp_year: string,
    @Arg('cardType') cardType: string,
    @Arg('lastFourDigit') lastFourDigit: string,
    @Arg('name') name: string
  ): Promise<any> {
    let customer: any = await CustomerModel.findById(new ObjectId(userId));
    let card = JSON.parse(customer.card);
    card.push({
      id: cardId,
      type: card.length === 0 ? 'primary' : 'secondary',
      exp_month: exp_month,
      exp_year: exp_year,
      cardType: cardType.toLocaleLowerCase(),
      lastFourDigit: lastFourDigit,
      name: name
    })
    customer.card = JSON.stringify(card);
    await CustomerModel.updateOne({ _id: new ObjectId(userId) }, customer);
    return customer;
  }

  @Mutation(() => Customer, { description: 'change primary card of customer' })
  async changePrimaryCard(
    @Arg('userId') userId: string,
    @Arg('cardId') cardId: string
  ): Promise<any> {
    let customer: any = await CustomerModel.findById(new ObjectId(userId));
    let card = JSON.parse(customer.card);
    card.forEach((ele: any) => {
      ele.type = 'secondary';
      if (ele.id === cardId) {
        ele.type = 'primary';
      }
    });
    customer.card = card;
    customer.card = JSON.stringify(card);
    await CustomerModel.updateOne({ _id: new ObjectId(userId) }, customer);
    return customer;
  }

  @Mutation(() => Customer, { description: "delete customer's card" })
  async deleteCard(
    @Arg('userId') userId: string,
    @Arg('cardId') cardId: string
  ): Promise<any> {
    let customer: any = await CustomerModel.findById(new ObjectId(userId));
    let card = JSON.parse(customer.card);
    let newContact = card.filter((ele: any) => ele.id !== cardId);
    customer.card = JSON.stringify(newContact);
    await CustomerModel.updateOne({ _id: new ObjectId(userId) }, customer);
    return customer;
  }

  @Mutation(() => Customer, {description: "change user's information"})
  async updateUserInfor(
    @Arg('userId') userId: string,
    @Arg('name') name: string,
    @Arg('email') email: string
  ): Promise<any> {
    let customer: any = await CustomerModel.findById(new ObjectId(userId));
    customer.name = name;
    customer.email = email;
    await CustomerModel.updateOne({ _id: new ObjectId(userId) }, customer);
    return customer;
  }
}
