import { ObjectType, Field, Int } from 'type-graphql';


@ObjectType()
export default class Sms {
    @Field()
    sid: string;

    @Field()
    date_created: string;

    @Field()
    date_updated: string;

    @Field({ nullable: true })
    date_sent: string;

    @Field()
    account_sid: string;

    @Field()
    to: string;

    @Field()
    from: string;

    @Field({ nullable: true })
    messaging_service_sid: string;

    @Field()
    body: string;

    @Field()
    status: string;

    @Field()
    num_segments: string;

    @Field()
    num_media: string;

    @Field()
    direction: string;

    @Field()
    api_version: string;

    @Field({ nullable: true })
    price: string;

    @Field()
    price_unit: string;

    @Field()
    error_code: string;

    @Field()
    error_message: string;

    @Field()
    uri: string;
}

