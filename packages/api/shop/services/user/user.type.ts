import { ObjectType, Field, Int } from 'type-graphql';
import Address from '../../types/address.type';
import Contact from '../../types/contact.type';
import Card from '../../types/card.type';

@ObjectType()
export default class User {
  @Field(type => Int)
  id: number;

  @Field()
  name: string;

  @Field()
  token: string;
  
  @Field()
  email: string;

  @Field(type => [Address])
  address: Address[];

  @Field(type => [Contact])
  contact: Contact[];

  @Field(type => [Card])
  card: Card[];
}
