import { InputType, Field } from "type-graphql";
import { Length } from "class-validator";
import Address from "../../types/address.type";

@InputType()
export default class AddressInput implements Partial<Address> {

    @Field(() => String)
    id!: string;

    @Field(() => String)
    @Length(1, 255)
    type!: string;

    @Field(() => String)
    name!: string;

    @Field(() => String)
    info!: string;

}