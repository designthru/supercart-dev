import { Resolver, Arg, Mutation } from 'type-graphql';
import Payment from './payment.type';
import { Customer, CustomerModel } from '../../entities/Customer';
import { ObjectId } from 'mongodb'
import StripeCoupon from './coupon.type';
import Stripe from 'stripe';
const stripe = new Stripe(process.env.STRIPE_SECRET_KEY || "sk_test_3b9jDhvluCnulrhG8dQs2olE00VdqBdG5m", { apiVersion: '2020-03-02'});

@Resolver()
export class PaymentResolver {
  @Mutation(() => Payment, { description: 'Charge a Payment' })
  async charge(@Arg('paymentInput') paymentInput: string): Promise<Payment> {
    console.log(JSON.parse(paymentInput), 'paymentInput');
    return await {
      status: 200,
    };
  }

  @Mutation(() => Customer, {description: "Create customer in Stripe"})
  async createCustomer(
    @Arg('id') id: string,
    @Arg('email') email: string,
    @Arg('name') name: string,
    @Arg('phone') phone: string,
    @Arg('source') source: string,
    @Arg('coupon') coupon: string,
    @Arg('totalPrice') totalPrice: string,
    @Arg('totalOrder') totalOrder: string
  ): Promise<any> {
    try {
      let currentCustomer: any = await CustomerModel.findById(new ObjectId(id));
      console.log("currentCustomer", currentCustomer, totalPrice, totalOrder);
      if (!currentCustomer.customerId) {
        console.log("Came here....")
        let stripeData = await stripe.customers.create(
          {
            email: email,
            name: name,
            phone: phone,
            source: source
          }
        );
        currentCustomer.customerId = stripeData.id;
        console.log(currentCustomer);
      }
      const amount = Math.floor(Number(totalPrice) * 100);
      let charge = await stripe.charges.create(
        {
          amount: amount,
          currency: 'usd',
          description: 'Checkout for supercart',
          customer: currentCustomer.customerId
        }
      );
      console.log("charge ->", charge);
      currentCustomer.totalOrder += Number(totalOrder);
      currentCustomer.totalOrderAmount += Number(charge.amount)/100;
      await CustomerModel.updateOne({_id: new ObjectId(id)}, currentCustomer);
      return currentCustomer;
    // console.log(stripeData);
    }catch(err) {
      if (err.message.indexOf('You cannot use a Stripe token more than once') >= 0) {
        throw new Error('You can not use this card again. Please try to remove and add again.')
      } if (err.message.indexOf('No such customer') >= 0) {
        throw new Error('No such customer. please contact us.')
      }
      console.log(err.message);
    }
  }

  @Mutation(() => StripeCoupon, {description: 'Get coupon object'})
  async getCoupon(
    @Arg('id') id: string
  ): Promise<any>{
    try {
      let coupon = await stripe.coupons.retrieve(
        id
      );
      console.log(coupon);
      let data = {
        code: coupon.id,
        name: coupon.name,
        discountInPercent: coupon.percent_off,
        discountInFixed: coupon.amount_off
      }
      return data;
    } catch(err) {
      console.log(err);
      throw new Error(err.message)
    }
  }
}
