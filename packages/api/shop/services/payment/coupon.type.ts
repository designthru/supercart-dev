import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export default class StripeCoupon {
    @Field()
    code: string

    @Field()
    name: string;

    @Field({nullable: true})
    discountInPercent: number;

    @Field({ nullable: true })
    discountInFixed: number;
    
}
