import { InputType, Field } from 'type-graphql';

@InputType()
export default class CustomerInput {
    @Field()
    token: string;

    @Field()
    amount: number;
}
