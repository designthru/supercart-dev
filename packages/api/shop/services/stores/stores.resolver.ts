import { Resolver, Query, Arg, Args } from 'type-graphql';
import { storeSamples } from './stores.sample';
import { Store } from './store.type';
import { Stores } from './stores.type';
import { GetStoresArgs } from './get-stores.args';
import search from '../../helpers/search';

@Resolver()
export class StoreResolver {
  private readonly storeCollection: Store[] = storeSamples;

  @Query(() => Stores, { description: 'Get all the stores' })
  async stores(
    @Args() { offset, limit, text, type, category }: GetStoresArgs
  ): Promise<Stores | undefined> {
    let items = this.storeCollection;

    if (category) {
      items = items.filter((item) => item.categories.includes(category));
    }
    items = await search(items, ['name'], text);

    const total = this.storeCollection.length;
    const hasMore = this.storeCollection.length > offset + limit;

    return {
      items: items.slice(offset, offset + limit),
      totalCount: total,
      hasMore,
    };
  }

  @Query(() => Store)
  async store(
    @Arg('slug', (type) => String) slug: string
  ): Promise<Store | undefined> {
    return await this.storeCollection.find((item) => item.slug === slug);
  }
}
