import { ObjectType, Field, Int } from 'type-graphql';
import { Store } from './store.type';
@ObjectType()
export class Stores {
  @Field(type => [Store], { nullable: true })
  items?: Store[];

  @Field(type => Int, { defaultValue: 0 })
  totalCount: number;

  @Field()
  hasMore: boolean;
}
