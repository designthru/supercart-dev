import mongoose from 'mongoose';

module.exports = () => {
  mongoose
    .connect( 'mongodb+srv://supercartdb-kxbaj.mongodb.net/', {
      dbName: process.env.DB_NAME,
      user: process.env.DB_USER,
      pass:process.env.DB_PASS,
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true
    })
    .then(() => {
      console.log('Mongodb connected.....');
    })
    .catch(err => console.log(err.message));

  mongoose.connection.on('connected', () => {
    console.log('Mongoose connected to db...');
  });

  mongoose.connection.on('disconnected', () => {
    console.log('Mongoose connection is disconnected...');
  });

  process.on('SIGINT', () => {
    mongoose.connection.close(() => {
      console.log(
        'Mongoose connection is disconnected due to app termination...'
      );
      process.exit(0);
    });
  });

}