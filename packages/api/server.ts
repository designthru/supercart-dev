import 'reflect-metadata';
import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { buildSchema } from 'type-graphql';

//resolvers for shop part
import { UserResolver } from './shop/services/user/user.resolver';
import { ProductResolver } from './shop/services/product/product.resolver';
import { PaymentResolver } from './shop/services/payment/payment.resolver';
import { OrderResolver } from './shop/services/order/order.resolver';
import { CouponResolver } from './shop/services/coupon/coupon.resolver';
import { CategoryResolver } from './shop/services/category/category.resolver';
import { StoreResolver } from './shop/services/stores/stores.resolver';

//resolvers for admin part
import AdminProductResolver from './admin/services/product/product.resolver';
import AdminCategoryResolver from './admin/services/category/category.resolver';
import AdminCustomerResolver from './admin/services/customer/customer.resolver';
import AdminCouponResolver from './admin/services/coupon/coupon.resolver';
import AdminOrderResolver from './admin/services/order/order.resolver';
import AdminStaffResolver from './admin/services/staff/staff.resolver';

import * as dotenv from 'dotenv';
import mongoose from 'mongoose';
mongoose.set('useCreateIndex', true);

const app: express.Application = express();
const path = '/supercart/graphql';
dotenv.config();
const main = async () => {
  const schema = await buildSchema({
    resolvers: [
      UserResolver,
      ProductResolver,
      PaymentResolver,
      OrderResolver,
      CouponResolver,
      CategoryResolver,
      StoreResolver,
      AdminProductResolver,
      AdminCategoryResolver,
      AdminCustomerResolver,
      AdminCouponResolver,
      AdminOrderResolver,
      AdminStaffResolver
    ],
  });
  const apolloServer = new ApolloServer({
    schema,
    introspection: true,
    playground: true,
    tracing: true,
  });
  apolloServer.applyMiddleware({ app, path });

  app.listen(process.env.PORT, () => {
    require('./initDB')();
    console.log(`🚀 started http://localhost:${process.env.PORT}${path}`);
  });
};

main();
