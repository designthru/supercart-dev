import React, { useContext, useState, useEffect } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { Col } from 'react-styled-flexboxgrid';
import { openModal } from '@redq/reuse-modal';
import RadioCard from 'components/RadioCard/RadioCard';
import { ProfileContext } from 'contexts/profile/profile.context';
import StripePaymentForm from '../../Payment/StripePaymentForm';
import {
  SettingsForm,
  SettingsFormContent,
  HeadingSection,
  Title,
  Input,
  Row,
  ButtonGroup,
} from './Settings.style';
import RadioGroup from 'components/RadioGroup/RadioGroup';
import PaymentGroup from 'components/PaymentGroup/PaymentGroup';
import UpdateAddress from '../../CheckoutWithSidebar/Update/UpdateAddress';
import UpdateContact from '../../CheckoutWithSidebar/Update/UpdateContact';
import Button from 'components/Button/Button';
import { 
  SIGN_UP_FROM_CHECKOUT,
  CHANGE_PRIMARY_ADDRESS,
  CHANGE_PRIMARY_CONTACT,
  CHANGE_PRIMARY_CARD,
  DELETE_CARD,
  DELETE_CONTACT,
  DELETE_ADDRESS,
  CHANGE_USER_INFO
 } from 'graphql/mutation/user';

import { FormattedMessage } from 'react-intl';
import { Label } from 'containers/ProductDetailsFood/ProductDetailsFood.style';
import  SmsModal  from './SmsModal';
import { ToastContainer, toast } from 'react-toastify';
import { AuthContext } from 'contexts/auth/auth.context';
import Router, { useRouter } from 'next/router';

type SettingsContentProps = {
  deviceType?: {
    mobile: boolean;
    tablet: boolean;
    desktop: boolean;
  };
};

const SettingsContent: React.FC<SettingsContentProps> = ({ deviceType }) => {
  const { state, dispatch } = useContext(ProfileContext);
  const { authState, authDispatch } = useContext<any>(AuthContext);

  
  const currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
  console.log("opoooo...", state);
  const [changeUserInfo, {loading: changeUserInfoLoading, error: changeUserInfoError}] = useMutation(CHANGE_USER_INFO);
  const [deleteAddressMutation, { loading: deleteAddressLoading, error: deleteAddressError }] = useMutation(DELETE_ADDRESS);
  const [deleteContactMutation, { loading: deleteContactLoading, error: deleteContactError }] = useMutation(DELETE_CONTACT);
  const [changePrimaryContactMutation, { loading: changePrimaryContactLoading, error: changePrimaryContactError }] = useMutation(CHANGE_PRIMARY_CONTACT);
  const [changePrimaryAddressMutation, { loading: changePrimaryAddressLoading, error: changePrimaryAddressError }] = useMutation(CHANGE_PRIMARY_ADDRESS);
  const [deleteCardMutation, { loading: deleteCarLoading, error: deleteCardError }] = useMutation(DELETE_CARD);

  const [address, setAddress] = useState(currentCustomer ? currentCustomer.address : []);
  const [contact, setContact] = useState(currentCustomer ? currentCustomer.contact : []);
  const [card, setCard] = useState(currentCustomer ? currentCustomer.card : []);
  const [name, setName] = useState(currentCustomer ? currentCustomer.name : '');
  const [email, setEmail] = useState(currentCustomer ? currentCustomer.email : '');

  const showErrorMessage = (msg) => {
    toast.error(msg, {
      position: "top-center",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  }

  //error handling for change primary address mutation
  if (changePrimaryAddressError) {
    showErrorMessage('Something went wrong while changing primary address');
  }

  //error handling for deleting address
  if (deleteAddressError) {
    showErrorMessage('Something went wrong while deleting address');
  }

  //error handling for deleting contact
  if (deleteContactError) {
    showErrorMessage('Something went wrong while deleting contact');
  }

  //error handling for changing primary contact
  if (changePrimaryContactError) {
    showErrorMessage('Something went wrong while chaning primary contact');
  }

  // const { address, contact, card } = state;
  useEffect(() => {
    return () => {
      console.log("in useeffect function..")
      if (!authState.isAuthenticated) {
        console.log("not loggedin");
        Router.push('/');
        return;
      }
      const currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
      setAddress(currentCustomer.address || []);
      setCard(currentCustomer.card || []);
      setContact(currentCustomer.contact || []);
    };
  }, [authState])
  const handleChange = async(value: string, field: string) => {
    dispatch({ type: 'HANDLE_ON_INPUT_CHANGE', payload: { value, field } });
  };
  // Add or edit modal
  const handleModal = (
    modalComponent: any,
    modalProps = {},
    className: string = 'add-address-modal'
  ) => {
    openModal({
      show: true,
      config: {
        width: 360,
        height: 'auto',
        enableResizing: false,
        disableDragging: true,
        className: className,
      },
      closeOnClickOutside: true,
      component: modalComponent,
      componentProps: { item: modalProps },
    });
  };

  const handleSendSms = async() => {
    if (contact.length === 0) {
      toast.error('Please add at least 1 phone number.', {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else {
      handleModal(SmsModal, state);
    }
    console.log(contact.length);
  }

  const handleEditDelete = async (item: any, type: string, name: string) => {
    if (type === 'edit') {
      const modalComponent = name === 'address' ? UpdateAddress : UpdateContact;
      handleModal(modalComponent, item);
    } else {
      console.log(name, item, type, 'delete');
      switch (name) {
        case 'payment':
          if (authState.isAuthenticated) {
            authDispatch({
              type: 'DELETE_CARD', payload: item.id
            });
            // let deleteCard = await deleteCardMutation({
            //   variables: {
            //     userId: authState.currentCustomer._id,
            //     cardId: item.id
            //   }
            // });
            toast.success('Card deleted', {
              position: "top-center",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            dispatch({ type: 'DELETE_CARD', payload: item.id });
          }
          return true;
        case 'contact':
          if (authState.isAuthenticated) {
            authDispatch({
              type: 'DELETE_CONTACT', payload: item.id
            });
            let delContact = await deleteContactMutation(
              {
                variables: { userId: authState.currentCustomer._id, contactId: item.id }
              }
            );
            toast.success('Contact deleted', {
              position: "top-center",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            dispatch({ type: 'DELETE_CONTACT', payload: item.id });
          }
          return true;
        case 'address':
          if (authState.isAuthenticated) {
            authDispatch({ type: 'DELETE_ADDRESS', payload: item.id });
            let delAddress = await deleteAddressMutation({
              variables: { userId: authState.currentCustomer._id, addressId: item.id }
            });
            toast.success('Address deleted', {
              position: "top-center",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            dispatch({ type: 'DELETE_ADDRESS', payload: item.id });
          }
        default:
          return false;
      }
    }
  };

  const handleSave = async () => {
    console.log(name, email);
    authDispatch({
      type: 'CHANGE_USER_INFO',
      payload: {
        name: name,
        email: email,
        userId: currentCustomer._id
      }
    })
    let updateData = await changeUserInfo({
      variables: {
        userId: currentCustomer._id,
        name: name,
        email: email
      }
    });
    toast.success("User information was changed", {
      position: "top-center",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    console.log("response from server..", updateData);
  };

  return (
    <SettingsForm>
      <SettingsFormContent>
        <HeadingSection>
          <Title>
            <FormattedMessage
              id="profilePageTitle"
              defaultMessage="Your Profile"
            />
          </Title>
        </HeadingSection>
        <Row style={{ alignItems: 'flex-end', marginBottom: '50px' }}>
          <Col xs={12} sm={5} md={5} lg={5}>
            <Input
              type="text"
              label="Name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              style={{ backgroundColor: '#F7F7F7' }}
              intlInputLabelId="profileNameField"
            />
          </Col>

          <Col xs={12} sm={5} md={5} lg={5}>
            <Input
              type="email"
              label="Email Address"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              style={{ backgroundColor: '#F7F7F7' }}
              intlInputLabelId="profileEmailField"
            />
          </Col>

          <Col xs={12} sm={2} md={2} lg={2}>
            <Button
              title="Save"
              onClick={handleSave}
              style={{ width: '100%' }}
              intlButtonId="profileSaveBtn"
              isLoading={changeUserInfoLoading}
            />
          </Col>
        </Row>
        {/* <Row>
          <Col xs={12} sm={12} md={12} Lg={12}>
            <SettingsFormContent>
              <HeadingSection>
                <Title>
                  <FormattedMessage
                    id="verificationStatus"
                    defaultMessage="Verification Status"
                  ></FormattedMessage>
                </Title>
              </HeadingSection>
              <Row>
                <Col xs={12} md={6} sm={6} lg={6}>
                  <Label>Email verification: </Label>
                  {
                    !state.isEmailVerified &&
                    <Label style={{color: 'red'}}>Not verified</Label>
                  }
                  {
                    !state.isEmailVerified &&
                    <a style={{ marginLeft: 10, color: '#009E7F', cursor: 'pointer'}}>Verify now</a>
                  }
                </Col>
                <Col xs={12} md={6} sm={6} lg={6}>
                  <Label>Phone verification: </Label>
                  {
                    !state.isPhoneVerified &&
                    <Label style={{ color: 'red' }}>Not verified</Label>
                  }
                  {
                    !state.isPhoneVerified &&
                    <a onClick={handleSendSms} style={{ marginLeft: 10, color: '#009E7F', cursor: 'pointer' }}>Verify now</a>
                  }
                </Col>
              </Row>
            </SettingsFormContent>
            <ToastContainer />
          </Col>
        </Row> */}
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            <SettingsFormContent>
              <HeadingSection>
                <Title>
                  <FormattedMessage
                    id="contactNumberTItle"
                    defaultMessage="Contact Numbers"
                  />
                </Title>
              </HeadingSection>
              <ButtonGroup>
                <RadioGroup
                  items={contact}
                  component={(item: any) => (
                    <RadioCard
                      id={item.id}
                      key={item.id}
                      title={item.type}
                      content={item.number}
                      checked={item.type === 'primary'}
                      onChange={async () => {
                        if (authState.isAuthenticated) {
                          authDispatch({
                            type: 'SET_PRIMARY_CONTACT',
                            payload: item.id.toString()
                          });
                          let data = await changePrimaryContactMutation({
                            variables: { userId: authState.currentCustomer._id, contactId: item.id }
                          });
                          toast.success('Primary contact changed', {
                            position: "top-center",
                            autoClose: 3000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                          });
                        } else {
                          dispatch({
                            type: 'SET_PRIMARY_CONTACT',
                            payload: item.id.toString(),
                          })
                        }
                      }}
                      name="contact"
                      onEdit={() => handleEditDelete(item, 'edit', 'contact')}
                      onDelete={() =>
                        handleEditDelete(item, 'delete', 'contact')
                      }
                    />
                  )}
                  secondaryComponent={
                    <Button
                      title="Add Contact"
                      iconPosition="right"
                      colors="primary"
                      size="small"
                      variant="outlined"
                      type="button"
                      intlButtonId="addContactBtn"
                      onClick={() =>
                        handleModal(UpdateContact, {}, 'add-contact-modal')
                      }
                    />
                  }
                />
              </ButtonGroup>
            </SettingsFormContent>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12} style={{ position: 'relative' }}>
            <SettingsFormContent>
              <HeadingSection>
                <Title>
                  <FormattedMessage
                    id="deliveryAddresTitle"
                    defaultMessage="Delivery Address"
                  />
                </Title>
              </HeadingSection>
              <ButtonGroup>
                <RadioGroup
                  items={address}
                  component={(item: any) => (
                    <RadioCard
                      id={item.id}
                      key={item.id}
                      title={item.name}
                      content={item.info}
                      name="address"
                      checked={item.type === 'primary'}
                      onChange={async () => {
                        if (authState.isAuthenticated) {
                          authDispatch({
                            type: 'SET_PRIMARY_ADDRESS',
                            payload: item.id.toString()
                          });
                          let data = await changePrimaryAddressMutation({
                            variables: { userId: authState.currentCustomer._id, addressId: item.id }
                          });
                          toast.success('Primary address changed', {
                            position: "top-center",
                            autoClose: 3000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                          });
                        } else {
                          dispatch({
                            type: 'SET_PRIMARY_ADDRESS',
                            payload: item.id.toString(),
                          });
                        }
                      }}
                      onEdit={() => handleEditDelete(item, 'edit', 'address')}
                      onDelete={() =>
                        handleEditDelete(item, 'delete', 'address')
                      }
                    />
                  )}
                  secondaryComponent={
                    <Button
                      title="Add Adderss"
                      iconPosition="right"
                      colors="primary"
                      size="small"
                      variant="outlined"
                      type="button"
                      intlButtonId="addAddressBtn"
                      onClick={() =>
                        handleModal(UpdateAddress, {}, 'add-address-modal')
                      }
                    />
                  }
                />
              </ButtonGroup>
            </SettingsFormContent>
          </Col>
        </Row>

        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            <SettingsFormContent>
              <HeadingSection>
                <Title>
                  <FormattedMessage
                    id="paymentCardTitle"
                    defaultMessage="Payments Card"
                  />
                </Title>
              </HeadingSection>
              <PaymentGroup
                name="payment"
                deviceType={deviceType}
                items={card}
                onEditDeleteField={(item: any, type: string) =>
                  handleEditDelete(item, type, 'payment')
                }
                onChange={(item: any) =>
                  dispatch({
                    type: 'SET_PRIMARY_CARD',
                    payload: item.id.toString(),
                  })
                }
                handleAddNewCard={() => {
                  handleModal(
                    StripePaymentForm,
                    { buttonText: 'Add Card' },
                    'add-address-modal stripe-modal'
                  );
                }}
              />
            </SettingsFormContent>
          </Col>
        </Row>
      </SettingsFormContent>
      <ToastContainer />
    </SettingsForm>
  );
};

export default SettingsContent;
