import React, { useContext, useState } from 'react';
import {
    Input
} from '../../SignInOutForm/SignInOutForm.style';
import Router from 'next/router';
import styled from 'styled-components';
import { themeGet } from '@styled-system/theme-get';
import * as Yup from 'yup';
import { closeModal } from '@redq/reuse-modal';
import { FormikProps, ErrorMessage, Formik, Form } from 'formik';
import { useMutation } from '@apollo/react-hooks';
import MaskedInput from 'react-text-mask';
import { ProfileContext } from 'contexts/profile/profile.context';
import Button from 'components/Button/Button';
import { SEND_SMS } from 'graphql/mutation/sms';
import { DO_PHONE_VERIFY } from 'graphql/mutation/user';
import { FieldWrapper, Heading } from '../../CheckoutWithSidebar/Update/Update.style';
import { useCart } from 'contexts/cart/use-cart';
import { AuthContext } from 'contexts/auth/auth.context';
import { ToastContainer, toast } from 'react-toastify';
type Props = {
    item?: any | null;
};
// Shape of form values
type FormValues = {
    id?: number | null;
    type?: string;
    number?: string;
};

const ContactValidationSchema = Yup.object().shape({
    number: Yup.string().required('Code is incorrect'),
});

const SmsModal: React.FC<Props> = ({ item }) => {
    const initialValues = {
        id: item.id || null,
        type: item.type || 'secondary',
        number: item.number || '',
    };
    const { clearCart } = useCart();
    const [isCodeSent, setIsCodeSent] = useState(false);    //flag to check if code is sent
    const [code, setCode] = useState("");   //code from user's input
    const [genCode, setGenCode] = useState(""); //code generagted by program
    const [isCodeMatched, setIsCodeMatched] = useState(true);  //flag to check if user's input is same with generated code
    const [primaryPhone, setPrimaryPhone] = useState(item.number || '');
    const [isPhoneSet, setIsPhoneSet] = useState(false);
    // const primaryPhone = item.number || '';
    const [sendSmsMutation, {loading: sendSmsLoading, error: sendSmsError}] = useMutation(SEND_SMS);
    const [doPhoneVerify, {loading: phoneVerifyLoading, error: phoneVerifyError}] = useMutation(DO_PHONE_VERIFY);
    if (sendSmsError) {
        toast.error('Something went wrong while sending verification code.', {
            position: "top-center",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined
        });
    }
    const { authState, authDispatch } = useContext<any>(AuthContext);
    const handleSubmit = async (values: FormValues) => {
        if (code === genCode) {
            setIsCodeMatched(true);
            // clearCart();
            // Router.push('/order-received');
            const result = await doPhoneVerify({
                variables: {id: item.id}
            });
            console.log("result is...", result);
            const address = JSON.parse(result.data.doPhoneVerify.customer.address);
            const contact = JSON.parse(result.data.doPhoneVerify.customer.contact);
            const card = JSON.parse(result.data.doPhoneVerify.customer.card);
            const customer = {
                _id: result.data.doPhoneVerify.customer._id,
                name: result.data.doPhoneVerify.customer.name,
                email: result.data.doPhoneVerify.customer.email,
                image: result.data.doPhoneVerify.customer.image,
                totalOrder: result.data.doPhoneVerify.customer.totalOrder,
                totalOrderAmount: result.data.doPhoneVerify.customer.totalOrderAmount,
                isEmailVerified: result.data.doPhoneVerify.customer.isEmailVerified,
                isPhoneVerified: result.data.doPhoneVerify.customer.isPhoneVerified,
                address: address,
                contact: contact,
                card: card
            }
            authDispatch({ type: 'SIGNUP_SUCCESS', payload: { customer: customer, token: result.data.doPhoneVerify.token}});
            closeModal();
        } else {
            setIsCodeMatched(false);
        }
    };
    const sendCode = async ({ setSubmitting }: any) => {
        setIsPhoneSet(true);
        setIsCodeSent(true);
        const generatedCode = generateCode();
        let data = await sendSmsMutation({
            variables: { phoneNumber: primaryPhone, code: generatedCode },
        });
        toast.success("We've sent 5 digits code to your phone number.", {
            position: "top-center",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined
        });
        setGenCode(generatedCode);
        setIsCodeMatched(true);
    };

    const generateCode = () => {
        const chars = '0123456789'.split('');
        let result = '';
        for (let i = 0; i < 6; i++) {
            var x = Math.floor(Math.random() * chars.length);
            result += chars[x];
        }
        return result;
    }
    return (
        <Formik
            initialValues={initialValues}
            onSubmit={handleSubmit}
            validationSchema={ContactValidationSchema}
        >
            {({
                values,
                handleChange,
                handleBlur,
                isSubmitting,
            }: FormikProps<FormValues>) => (
                    <Form>
                        <Heading>
                            Send Verification code
                        </Heading>
                        <p>As a security precaution, we will send a verification code to your secure phone number:
                            {
                                isPhoneSet && primaryPhone
                            }
                            {
                                !isPhoneSet &&
                                <MaskedInput
                                    mask={[
                                        '(',
                                        /[1-9]/,
                                        /\d/,
                                        /\d/,
                                        ')',
                                        ' ',
                                        /\d/,
                                        /\d/,
                                        /\d/,
                                        '-',
                                        /\d/,
                                        /\d/,
                                        /\d/,
                                        /\d/,
                                    ]}
                                    className='form-control'
                                    id='my-input-id'
                                    placeholder='Enter phone number'
                                    value={primaryPhone}
                                    onChange={e => setPrimaryPhone(e.target.value)}
                                    style={{ height: "37px", width: "auto", padding: "10px" }}
                                    render={(ref: any, props: {}) => (
                                        <StyledInput ref={ref} {...props} />
                                    )}
                                    required
                                />
                            }
                        </p>
                        {isCodeSent &&
                            <FieldWrapper>
                                <MaskedInput
                                    mask={[
                                        /[0-9]/,
                                        /[0-9]/,
                                        /[0-9]/,
                                        /[0-9]/,
                                        /[0-9]/,
                                        /[0-9]/,
                                    ]}
                                    className='form-control'
                                    placeholder='Enter a code you received'
                                    guide={false}
                                    id='my-input-id'
                                    value={code}
                                    onChange={e => { setCode(e.target.value); }}
                                    onBlur={handleBlur}
                                    name='number'
                                    style={{ marginTop: 10, height: 40 }}
                                    render={(ref: any, props: {}) => (
                                        <StyledInput ref={ref} {...props} />
                                    )}
                                />
                            </FieldWrapper>
                        }
                        {!isCodeMatched &&
                            <ErrorMessage name='number' component={StyledError} />
                        }
                        {!isCodeSent &&
                            <Button
                                disabled={isSubmitting}
                                type='button'
                                title='Send Code'
                                size='small'
                                onClick={sendCode}
                                fullwidth={true}
                                style={{ marginTop: 10, width: 'auto' }}
                            />
                        }
                        {isCodeSent &&
                            <Button
                                disabled={code.length === 0}
                                onClick={handleSubmit}
                                type='submit'
                                title='Verify'
                                size='small'
                                fullwidth={true}
                                style={{ marginTop: 10, width: 'auto' }}
                            />
                        }
                        {isCodeSent &&
                            <Button
                                className='sendAgain'
                                title='Send again'
                                size='small'
                                variant='textButton'
                                type='button'
                                intlButtonId='sendAgain'
                                style={{ float: 'right' }}
                                onClick={sendCode}
                            />
                        }
                        <ToastContainer />
                    </Form>
                )}
        </Formik>
    );
};

export default SmsModal;

const StyledInput = styled.input`
  width: 100%;
  height: 54px;
  border-radius: 6px;
  font-family: ${themeGet('colors.fontFamily', 'Lato, sans-serif')};
  border: 1px solid ${themeGet('colors.borderColor', '#e6e6e6')};
  color: ${themeGet('colors.darkBold', '#0D1136')};
  font-size: 16px;
  line-height: 19px;
  font-weight: 400;
  padding: 0 18px;
  box-sizing: border-box;
  transition: border-color 0.25s ease;

  &:hover,
  &:focus {
    outline: 0;
  }

  &:focus {
    border-color: ${themeGet('colors.primary', '#009e7f')};
  }

  &::placeholder {
    color: ${themeGet('colorsdarkRegular', '#77798C')};
  }
`;

const StyledError = styled.div`
  color: red;
  padding-bottom: 10px;
  margin-top: -5px;
`;

