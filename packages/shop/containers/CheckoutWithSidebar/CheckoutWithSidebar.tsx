import React, { useContext, useState, useEffect } from 'react';
import Router from 'next/router';
import Link from 'next/link';
import Button from 'components/Button/Button';
import RadioCard from 'components/RadioCard/RadioCard';
import RadioGroup from 'components/RadioGroup/RadioGroup';
import PaymentGroup from 'components/PaymentGroup/PaymentGroup';
import Loader from 'components/Loader/Loader';
import UpdateAddress from './Update/UpdateAddress';
import UpdateContact from './Update/UpdateContact';
import StripePaymentForm from '../Payment/StripePaymentForm';
import { CURRENCY } from 'helper/constant';
import { openModal } from '@redq/reuse-modal';
import { Product } from 'interfaces';
import { useMutation } from '@apollo/react-hooks';
import { Scrollbars } from 'react-custom-scrollbars';
import { CREATE_CUSTOMER } from 'graphql/mutation/stripe'

import CheckoutWrapper, {
  CheckoutContainer,
  CheckoutInformation,
  InformationBox,
  DeliverySchedule,
  Heading,
  ButtonGroup,
  CheckoutSubmit,
  HaveCoupon,
  CouponBoxWrapper,
  CouponInputBox,
  Input,
  CouponCode,
  RemoveCoupon,
  ErrorMsg,
  TermConditionText,
  TermConditionLink,
  CartWrapper,
  CalculationWrapper,
  OrderInfo,
  Title,
  ItemsWrapper,
  Items,
  Quantity,
  Multiplier,
  ItemInfo,
  Price,
  TextWrapper,
  Text,
  Bold,
  Small,
  NoProductMsg,
} from './CheckoutWithSidebar.style';

import { Plus } from 'components/AllSvgIcon';

import Sticky from 'react-stickynode';
import { HeaderContext } from 'contexts/header/header.context';

import { ProfileContext } from 'contexts/profile/profile.context';
import { FormattedMessage } from 'react-intl';
import { useCart } from 'contexts/cart/use-cart';
import { GET_COUPON } from 'graphql/mutation/stripe';
import { APPLY_COUPON } from 'graphql/mutation/coupon';
import { SIGN_UP_FROM_CHECKOUT, CHANGE_PRIMARY_ADDRESS, DELETE_ADDRESS, DELETE_CONTACT, CHANGE_PRIMARY_CONTACT, CHANGE_PRIMARY_CARD, DELETE_CARD } from 'graphql/mutation/user';
import { useLocale } from 'contexts/language/language.provider';
import { AuthContext } from 'contexts/auth/auth.context';
import AuthenticationForm from '../SignInOutForm/Form';
import { ToastContainer, toast } from 'react-toastify';
import StripePaymentFormCheckout  from '../Payment/StripeForm'
import { getDiscount } from 'helper/utility';
// The type of props Checkout Form receives
interface MyFormProps {
  token: string;
  deviceType: any;
}

type CartItemProps = {
  product: Product;
};

const OrderItem: React.FC<CartItemProps> = ({ product }) => {
  const { id, quantity, title, name, unit, price, salePrice } = product;
  const displayPrice = salePrice ? salePrice : price;
  return (
    <Items key={id}>
      <Quantity>{quantity}</Quantity>
      <Multiplier>x</Multiplier>
      <ItemInfo>
        {name ? name : title} {unit ? `| ${unit}` : ''}
      </ItemInfo>
      <Price>
        {CURRENCY}
        {(displayPrice * quantity).toFixed(2)}
      </Price>
    </Items>
  );
};

const CheckoutWithSidebar: React.FC<MyFormProps> = ({ token, deviceType }) => {
  const [hasCoupon, setHasCoupon] = useState(false);
  const [couponCode, setCouponCode] = useState('');
  const [couponError, setError] = useState('');
  const { state, dispatch } = useContext(ProfileContext);
  const { authState, authDispatch } = useContext<any>(AuthContext);

  const { isRtl } = useLocale();
  const {
    items,
    removeCoupon,
    coupon,
    applyCoupon,
    clearCart,
    cartItemsCount,
    calculatePrice,
    calculateDiscount,
    calculateDeliveryFee,
    calculateSubTotalPrice,
    isRestaurant,
    toggleRestaurant,
  } = useCart();
  const [loading, setLoading] = useState(false);
  const [isValid, setIsValid] = useState(false);
  const [address, setAddress] = useState([]);
  const [contact, setContact] = useState([]);
  const [card, setCard] = useState([]);
  const [currentCoupon, setCurrentCoupon] =useState('');
  // const [currentCustomer, setCurrentCustomer] = useState(authState.currentCustomer);
  // const address, contact, schedules;
  const { schedules } = state;
  const initData = () => {
    // setCurrentCustomer(JSON.parse(window.localStorage.getItem('currentCustomer')));
    // currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
    if (localStorage.getItem('userToken')) {
      setAddress(authState.currentCustomer.address);
      setContact(authState.currentCustomer.contact);
      setCard(authState.currentCustomer.card);
      if (
        calculatePrice() > 0 &&
        cartItemsCount > 0 &&
        authState.currentCustomer.address.length &&
        authState.currentCustomer.contact.length &&
        authState.currentCustomer.card
      ) {
        setIsValid(true);
      }
    } else {
      setAddress(state.address);
      setContact(state.contact);
      setCard(state.card);
      if (
        address.length &&
        contact.length &&
        // card.length &&
        schedules.length
      ) {
        setIsValid(true);
      }
    }
  }
  const [deleteCardMutation, {loading: deleteCarLoading, error: deleteCardError}] = useMutation(DELETE_CARD);
  
  const [testMutation] = useMutation(SIGN_UP_FROM_CHECKOUT);
  const [changePrimaryAddressMutation, {loading: changePrimaryAddressLoading, error: changePrimaryAddressError}] = useMutation(CHANGE_PRIMARY_ADDRESS);
  const [deleteAddressMutation, {loading: deleteAddressLoading, error: deleteAddressError}] = useMutation(DELETE_ADDRESS);
  const [deleteContactMutation, {loading: deleteContactLoading, error: deleteContactError}] = useMutation(DELETE_CONTACT);
  const [changePrimaryContactMutation, {loading: changePrimaryContactLoading, error: changePrimaryContactError}] = useMutation(CHANGE_PRIMARY_CONTACT);
  const [changePrimaryCardMutation, {loading: changePrimaryCardLoading, error: changePrimaryCardError}] = useMutation(CHANGE_PRIMARY_CARD);
  const [createCustomer, {loading: createCustomerLoading, error: createCustomerError}] = useMutation(CREATE_CUSTOMER);
  const [getCoupon, {error: getCouponError, loading: getCouponLoading}] = useMutation(GET_COUPON);
  const { headerState } = useContext<any>(HeaderContext);

  const totalHeight =
    headerState?.desktopHeight > 0 ? headerState.desktopHeight + 30 : 76 + 30;

  const handleSubmit = async () => {
    let add = {
      id: '123',
      name: 'name'
    };
  
    // const number = {contact};
    if (!state._id) {
      // dispatch()
      authDispatch({
        type: 'SIGNUP',
      });

      openModal({
        show: true,
        overlayClassName: 'quick-view-overlay',
        closeOnClickOutside: true,
        component: AuthenticationForm,
        closeComponent: '',
        config: {
          enableResizing: false,
          disableDragging: true,
          className: 'quick-view-modal',
          width: 458,
          height: 'auto',
        },
      });
    } else {
      
      if (calculatePrice() < 75) {
        const differences = 75 - calculateSubTotalPrice();
        showErrorMessage(`Minimum order amount is $75.00. You need ${differences} worth of items to process your order.`);
        return;
      }
      setLoading(true);
      const primaryCard = card.find(ele => ele.type === 'primary');
      const primaryContact = contact.find(ele => ele.type === 'primary');
      await createCustomer({
        variables: { id: state._id, email: state.email, phone: primaryContact.number, source: primaryCard.id, name: state.name, coupon: currentCoupon ? currentCoupon : 'none', totalPrice: calculatePrice().toString(), totalOrder: calculateSubTotalPrice().toString()}
      }).then(
        data => {
          setLoading(false);
          toast.success('Payment Success!', {
            position: "top-center",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            onClose: () => {
              removeCoupon();
              setHasCoupon(false);
              clearCart();
              Router.push('/order-received');
            }
          });
        }
      ).catch(
        err=> {
          setLoading(false);
          console.log(err.message)
          if (err.graphQLErrors.length) {
            showErrorMessage(err.graphQLErrors[0].message);
          } else {
            showErrorMessage(err.message);
          }
        }
      );
    }
  };

  const showErrorMessage = (msg) => {
    toast.error(msg, {
      position: "top-center",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  }

  //error handling for change primary address mutation
  if (changePrimaryAddressError) {
    showErrorMessage('Something went wrong while changing primary address');
  }

  //error handling for deleting address
  if (deleteAddressError) {
    showErrorMessage('Something went wrong while deleting address');
  }

  //error handling for deleting contact
  if (deleteContactError) {
    showErrorMessage('Something went wrong while deleting contact');
  }

  //error handling for changing primary contact
  if (changePrimaryContactError) {
    showErrorMessage('Something went wrong while chaning primary contact');
  }
  useEffect(() => {
    initData();
    console.log("after init...", calculatePrice(), address.length, contact.length, card.length);
  }, [authState]);

  useEffect(() => {
    return () => {
      if (isRestaurant) {
        toggleRestaurant();
        clearCart();
      }
    };
  }, []);
  // Add or edit modal
  const handleModal = (
    modalComponent: any,
    modalProps = {},
    className: string = 'add-address-modal'
  ) => {
    openModal({
      show: true,
      config: {
        width: 360,
        height: 'auto',
        enableResizing: false,
        disableDragging: true,
        className: className,
      },
      closeOnClickOutside: true,
      component: modalComponent,
      componentProps: { item: modalProps },
    });
  };

  //Open Sms modal
  const openSmsModal = (
    modalComponent: any,
    modalProps  = {},
    className: string = 'add-address-modal'
  ) => {
    openModal({
      show: true,
      config: {
        width: 450,
        height: 'auto',
        enableResizing: false,
        disableDragging: true,
        className: className,
      },
      closeOnClickOutside: true,
      component: modalComponent,
      componentProps: { item: modalProps },
    });
  }

  const handleEditDelete = async (item: any, type: string, name: string) => {
    if (type === 'edit') {
      const modalComponent = name === 'address' ? UpdateAddress : UpdateContact;
      handleModal(modalComponent, item);
    } else {
      switch (name) {
        case 'payment':
          if (authState.isAuthenticated) {
            authDispatch({
              type: 'DELETE_CARD', payload: item.id
            });
            let deleteCard = await deleteCardMutation({
              variables: {
                userId: authState.currentCustomer._id,
                cardId: item.id
              }
            });
            toast.success('Card deleted', {
              position: "top-center",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            dispatch({ type: 'DELETE_CARD', payload: item.id });
          }
          return true;
        case 'contact':
          if (authState.isAuthenticated) {
            authDispatch({
              type: 'DELETE_CONTACT', payload: item.id
            });
            let delContact = await deleteContactMutation(
              {
                variables: {userId: authState.currentCustomer._id, contactId: item.id}
              }
            );
            toast.success('Contact deleted', {
              position: "top-center",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            dispatch({ type: 'DELETE_CONTACT', payload: item.id });
          }
          return true;
        case 'address':
          if (authState.isAuthenticated) {
            authDispatch({ type: 'DELETE_ADDRESS', payload: item.id });
            let delAddress = await deleteAddressMutation({
              variables: { userId: authState.currentCustomer._id, addressId: item.id}
            });
            toast.success('Address deleted', {
              position: "top-center",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            dispatch({ type: 'DELETE_ADDRESS', payload: item.id });
          }
        default:
          return false;
      }
    }
  };

  const handleApplyCoupon = async () => {
    await getCoupon({
      variables: { id: couponCode },
    }).then(
      (data:any) => {
        console.log("response from mutation...", data)
        if ((data.data.getCoupon && data.data.getCoupon.discountInPercent) || (data.data.getCoupon.discountInFixed)) {
          applyCoupon(data.data.getCoupon);
          setCurrentCoupon(data.data.getCoupon.code)
          setCouponCode('');
        } else {
          setError('Invalid Coupon');
        }
      }
    ).catch(err => {
      setError('Invalid Coupon');
    });
    
  };
  const handleOnUpdate = (couponCode: any) => {
    setCouponCode(couponCode);
  };
  return (
    <form>
      <CheckoutWrapper>
        <CheckoutContainer>
          <CheckoutInformation>
            {/* DeliveryAddress */}
            <InformationBox>
              <Heading>
                <FormattedMessage
                  id='checkoutDeliveryAddress'
                  defaultMessage='Delivery Address'
                />
              </Heading>
              <ButtonGroup>
                <RadioGroup
                  items={address}
                  component={(item: any) => (
                    <RadioCard
                      id={item.id}
                      key={item.id}
                      title={item.name}
                      content={item.info}
                      name='address'
                      checked={item.type === 'primary'}
                      onChange={async() => {
                        if (authState.isAuthenticated) {
                          authDispatch({
                            type: 'SET_PRIMARY_ADDRESS',
                            payload: item.id.toString()
                          });
                          let data = await changePrimaryAddressMutation({
                            variables: { userId: authState.currentCustomer._id, addressId: item.id}
                          });
                          toast.success('Primary address changed', {
                            position: "top-center",
                            autoClose: 3000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                          });
                        } else {
                          dispatch({
                            type: 'SET_PRIMARY_ADDRESS',
                            payload: item.id.toString(),
                          });
                        }
                      }}
                      onEdit={() => handleEditDelete(item, 'edit', 'address')}
                      onDelete={() =>
                        handleEditDelete(item, 'delete', 'address')
                      }
                    />
                  )}
                  secondaryComponent={
                    <Button
                      className='addButton'
                      title='Add New'
                      icon={<Plus width='10px' />}
                      iconPosition='left'
                      colors='primary'
                      size='small'
                      variant='textButton'
                      type='button'
                      intlButtonId='addNew'
                      onClick={() =>
                        handleModal(UpdateAddress, 'add-address-modal')
                      }
                    />
                  }
                />
              </ButtonGroup>
            </InformationBox>

            {/* DeliverySchedule */}
            <InformationBox>
              <DeliverySchedule>
                <Heading>
                  <FormattedMessage
                    id='deliverySchedule'
                    defaultMessage='Select Your Delivery Schedule'
                  />
                </Heading>
                <RadioGroup
                  items={schedules}
                  component={(item: any) => (
                    <RadioCard
                      id={item.id}
                      key={item.id}
                      title={item.title}
                      content={item.time_slot}
                      name='schedule'
                      checked={item.type === 'primary'}
                      withActionButtons={false}
                      onChange={() =>
                        dispatch({
                          type: 'SET_PRIMARY_SCHEDULE',
                          payload: item.id.toString(),
                        })
                      }
                    />
                  )}
                />
              </DeliverySchedule>
            </InformationBox>

            {/* Contact number */}
            <InformationBox>
              <Heading>
                <FormattedMessage
                  id='contactNumberText'
                  defaultMessage='Select Your Contact Number'
                />
              </Heading>
              <ButtonGroup>
                <RadioGroup
                  items={contact}
                  component={(item: any) => (
                    <RadioCard
                      id={item.id}
                      key={item.id}
                      title={item.type}
                      content={item.number}
                      checked={item.type === 'primary'}
                      onChange={async() =>
                        {
                          if (authState.isAuthenticated) {
                            authDispatch({
                              type: 'SET_PRIMARY_CONTACT',
                              payload: item.id.toString()
                            });
                            let data = await changePrimaryContactMutation({
                              variables: { userId: authState.currentCustomer._id, contactId: item.id }
                            });
                            toast.success('Primary contact changed', {
                              position: "top-center",
                              autoClose: 3000,
                              hideProgressBar: false,
                              closeOnClick: true,
                              pauseOnHover: true,
                              draggable: true,
                              progress: undefined,
                            });
                          } else {
                            dispatch({
                              type: 'SET_PRIMARY_CONTACT',
                              payload: item.id.toString(),
                            })
                          }
                        }
                      }
                      name='contact'
                      onEdit={() => handleEditDelete(item, 'edit', 'contact')}
                      onDelete={() =>
                        handleEditDelete(item, 'delete', 'contact')
                      }
                    />
                  )}
                  secondaryComponent={
                    <Button
                      title='Add Contact'
                      icon={<Plus width='10px' />}
                      iconPosition='left'
                      colors='primary'
                      size='small'
                      variant='outlined'
                      type='button'
                      intlButtonId='addContactBtn'
                      onClick={() =>
                        handleModal(UpdateContact, 'add-contact-modal')
                      }
                    />
                  }
                />
              </ButtonGroup>
            </InformationBox>
            {/* PaymentOption */}

            <InformationBox
              className='paymentBox'
              style={{ paddingBottom: 30 }}
            >
              <Heading>
                <FormattedMessage
                  id='selectPaymentText'
                  defaultMessage='Select Payment Option'
                />
              </Heading>
              <PaymentGroup
                name='payment'
                deviceType={deviceType}
                items={card}
                onEditDeleteField={ (item: any, type: string) =>
                  handleEditDelete(item, type, 'payment')
                }
                onChange={async (item: any) =>
                  {
                    if (authState.isAuthenticated) {
                      authDispatch({
                        type: 'SET_PRIMARY_CARD',
                        payload: item.id
                      });
                      let primaryCard = await changePrimaryCardMutation({
                        variables: { userId: authState.currentCustomer._id, cardId: item.id}
                      });
                      toast.success('Primary card changed', {
                        position: "top-center",
                        autoClose: 3000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                      });
                    } else {
                      dispatch({
                        type: 'SET_PRIMARY_CARD',
                        payload: item.id.toString(),
                      }) 
                    }
                  }
                }
                handleAddNewCard={() => {
                  handleModal(
                    StripePaymentForm,
                    { totalPrice: calculatePrice() },
                    'add-address-modal stripe-modal'
                  );
                }}
              />

              {/* Coupon start */}
              {coupon ? (
                <CouponBoxWrapper>
                  <CouponCode>
                    <FormattedMessage id='couponApplied' />
                    <span>{coupon.code}</span>

                    <RemoveCoupon
                      onClick={(e) => {
                        e.preventDefault();
                        removeCoupon();
                        setHasCoupon(false);
                      }}
                    >
                      <FormattedMessage id='removeCoupon' />
                    </RemoveCoupon>
                  </CouponCode>
                </CouponBoxWrapper>
              ) : (
                <CouponBoxWrapper>
                  {!hasCoupon ? (
                    <HaveCoupon onClick={() => setHasCoupon((prev) => !prev)}>
                      <FormattedMessage
                        id='specialCode'
                        defaultMessage='Have a special code?'
                      />
                    </HaveCoupon>
                  ) : (
                    <>
                      <CouponInputBox>
                        <Input
                          onUpdate={handleOnUpdate}
                          value={couponCode}
                          intlPlaceholderId='couponPlaceholder'
                        />
                        <Button
                          onClick={handleApplyCoupon}
                          title='Apply'
                          intlButtonId='voucherApply'
                        />
                      </CouponInputBox>

                      {couponError && (
                        <ErrorMsg>
                          <FormattedMessage
                            id='couponError'
                            defaultMessage={couponError}
                          />
                        </ErrorMsg>
                      )}
                    </>
                  )}
                </CouponBoxWrapper>
              )}

              <TermConditionText>
                <FormattedMessage
                  id='termAndConditionHelper'
                  defaultMessage='By making this purchase you agree to our'
                />
                <Link href='#'>
                  <TermConditionLink>
                    <FormattedMessage
                      id='termAndCondition'
                      defaultMessage='terms and conditions.'
                    />
                  </TermConditionLink>
                </Link>
              </TermConditionText>

              {/* CheckoutSubmit */}
              <CheckoutSubmit>
                <Button
                  onClick={handleSubmit}
                  type='button'
                  disabled={!isValid}
                  title='Proceed to Checkout'
                  intlButtonId='proceesCheckout'
                  loader={<Loader />}
                  isLoading={loading}
                />
              </CheckoutSubmit>
            </InformationBox>
          </CheckoutInformation>
          <ToastContainer />
          <CartWrapper>
            <Sticky enabled={true} top={totalHeight} innerZ={999}>
              <OrderInfo>
                <Title>
                  <FormattedMessage
                    id='cartTitle'
                    defaultMessage='Your Order'
                  />
                </Title>

                <Scrollbars
                  universal
                  autoHide
                  autoHeight
                  autoHeightMax='390px'
                  renderView={(props) => (
                    <div
                      {...props}
                      style={{
                        ...props.style,
                        marginLeft: isRtl ? props.style.marginRight : 0,
                        marginRight: isRtl ? 0 : props.style.marginRight,
                        paddingLeft: isRtl ? 15 : 0,
                        paddingRight: isRtl ? 0 : 15,
                      }}
                    />
                  )}
                >
                  <ItemsWrapper>
                    {cartItemsCount > 0 ? (
                      items.map((item) => (
                        <OrderItem key={`cartItem-${item.id}`} product={item} />
                      ))
                    ) : (
                      <NoProductMsg>
                        <FormattedMessage
                          id='noProductFound'
                          defaultMessage='No products found'
                        />
                      </NoProductMsg>
                    )}
                  </ItemsWrapper>
                </Scrollbars>

                <CalculationWrapper>
                  <TextWrapper>
                    <Text>
                      <FormattedMessage
                        id='subTotal'
                        defaultMessage='Subtotal'
                      />
                    </Text>
                    <Text>
                      {CURRENCY}
                      {calculateSubTotalPrice()}
                    </Text>
                  </TextWrapper>

                  <TextWrapper>
                    <Text>
                      <FormattedMessage
                        id='intlOrderDetailsDelivery'
                        defaultMessage='Delivery Fee'
                      />
                    </Text>
                    <Text>
                      {CURRENCY}
                      {calculateDeliveryFee()}  
                    </Text>
                  </TextWrapper>

                  <TextWrapper>
                    <Text>
                      <FormattedMessage
                        id='discountText'
                        defaultMessage='Discount'
                      />
                    </Text>
                    <Text>
                      {CURRENCY}
                      {calculateDiscount()}
                    </Text>
                  </TextWrapper>

                  <TextWrapper style={{ marginTop: 20 }}>
                    <Bold>
                      <FormattedMessage id='totalText' defaultMessage='Total' />{' '}
                      <Small>
                        (
                        <FormattedMessage
                          id='vatText'
                          defaultMessage='Incl. VAT'
                        />
                        )
                      </Small>
                    </Bold>
                    <Bold>
                      {CURRENCY}
                      {calculatePrice()}
                    </Bold>
                  </TextWrapper>
                </CalculationWrapper>
              </OrderInfo>
            </Sticky>
          </CartWrapper>
        </CheckoutContainer>
      </CheckoutWrapper>
    </form>
  );
};

export default CheckoutWithSidebar;
