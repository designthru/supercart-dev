import React, { useContext } from 'react';
import * as Yup from 'yup';
import { withFormik, FormikProps, Form } from 'formik';
import { closeModal } from '@redq/reuse-modal';
import TextField from 'components/TextField/TextField';
import Button from 'components/Button/Button';
import { useMutation } from '@apollo/react-hooks';
import { ADD_ADDRESS, EDIT_ADDRESS } from 'graphql/mutation/user';
import { FieldWrapper, Heading } from './Update.style';
import { ProfileContext } from 'contexts/profile/profile.context';
import { AuthContext } from 'contexts/auth/auth.context';
import uuid from 'react-uuid';
// Shape of form values
interface FormValues {
  id?: number | null;
  name?: string;
  info?: string;
}

// The type of props MyForm receives
interface MyFormProps {
  item?: any | null;
}

// Wrap our form with the using withFormik HoC
const FormEnhancer = withFormik<MyFormProps, FormValues>({
  // Transform outer props into form values
  mapPropsToValues: props => {
    return {
      id: props.item.id || null,
      name: props.item.name || '',
      info: props.item.info || '',
    };
  },
  validationSchema: Yup.object().shape({
    name: Yup.string().required('Title is required!'),
    info: Yup.string().required('Address is required'),
  }),
  handleSubmit: values => {
    console.log(values, 'values');
    // do submitting things
  },
});

const UpdateAddress = (props: FormikProps<FormValues> & MyFormProps) => {
  const {
    isValid,
    item,
    values,
    touched,
    errors,
    dirty,
    handleChange,
    handleBlur,

    handleReset,
    isSubmitting,
  } = props;
  const addressValue = {
    id: values.id,
    type: 'secondary',
    name: values.name,
    info: values.info,
  };
  const { state, dispatch } = useContext(ProfileContext);
  const { authState, authDispatch } = useContext<any>(AuthContext);
  const [addAddressMutation, { loading: addAddressLoading, error: addAddressError }] = useMutation(ADD_ADDRESS);
  const [editAddressMutation, { loading: updateAddressLoading, error: updatetAddressError }] = useMutation(EDIT_ADDRESS);
  const handleSubmit = async () => {
    if (isValid) {
      // console.log('result..', addressValue, authState);  
      // const addressData = await addressMutation({
      //   variables: { addressInput: JSON.stringify(addressValue) },
      // });
      // console.log(addressData, 'address data');
      if (authState.isAuthenticated) {
        let currentCustomer = JSON.parse(localStorage.getItem('currentCustomer'));
        if (!addressValue.id) {
          addressValue.id = uuid();
          let address = await addAddressMutation({
            variables: { userId: currentCustomer._id, addressId: addressValue.id, type: addressValue.type, name: addressValue.name, info: addressValue.info}
          });
        } else {
          let address = await editAddressMutation({
            variables: { userId: currentCustomer._id, addressId: addressValue.id, type: addressValue.type, name: addressValue.name, info: addressValue.info }
          });
        }
        authDispatch({type: 'ADD_OR_UPDATE_ADDRESS', payload: addressValue});
        closeModal();
      } else {
        dispatch({ type: 'ADD_OR_UPDATE_ADDRESS', payload: addressValue });
        closeModal();
      }
    }
  };
  return (
    <Form>
      <Heading>{item && item.id ? 'Edit Address' : 'Add New Address'}</Heading>
      <FieldWrapper>
        <TextField
          id='name'
          type='text'
          placeholder='Enter Title'
          error={touched.name && errors.name}
          value={values.name}
          onChange={handleChange}
          onBlur={handleBlur}
        />
      </FieldWrapper>

      <FieldWrapper>
        <TextField
          id='info'
          as='textarea'
          placeholder='Enter Address'
          error={touched.info && errors.info}
          value={values.info}
          onChange={handleChange}
          onBlur={handleBlur}
        />
      </FieldWrapper>

      <Button
        onClick={handleSubmit}
        type='submit'
        title='Save Address'
        size='medium'
        fullwidth={true}
        isLoading={addAddressLoading}
      />
    </Form>
  );
};

export default FormEnhancer(UpdateAddress);
