import React, { useContext, useState } from 'react';
import styled from 'styled-components';
import { themeGet } from '@styled-system/theme-get';
import * as Yup from 'yup';
import { FormikProps, ErrorMessage, Formik, Form } from 'formik';
import { useMutation } from '@apollo/react-hooks';
import MaskedInput from 'react-text-mask';
import { ProfileContext } from 'contexts/profile/profile.context';
import Button from 'components/Button/Button';
import { UPDATE_CONTACT } from 'graphql/mutation/contact';
import { SEND_SMS } from 'graphql/mutation/sms';
import { FieldWrapper, Heading } from '../CheckoutWithSidebar/Update/Update.style';
import { useCart } from 'contexts/cart/use-cart';
import { AuthContext } from 'contexts/auth/auth.context';
import RadioCard from 'components/RadioCard/RadioCard';
import RadioGroup from 'components/RadioGroup/RadioGroup';
import { FormattedMessage } from 'react-intl';
import { openModal, closeModal } from '@redq/reuse-modal';
import { ToastContainer, toast } from 'react-toastify';
import uuid from 'react-uuid';
import { SEND_VERIFICATION_EMAIL } from 'graphql/mutation/user';
import SmsModal from '../Profile/Settings/SmsModal';

import { 
    InformationBox
 } from '../CheckoutWithSidebar/CheckoutWithSidebar.style';

type Props = {
    item?: any | null;
};
// Shape of form values
type FormValues = {
    id?: number | null;
    type?: string;
    number?: string;
};

const VerificationConfirmModal: React.FC<Props> = ({ item }) => {
    const initialValues = {
        email: item.email ? item.email : "",
        phone: item.phone ? item.phone.number : ""
    };
    const { clearCart } = useCart();
    const [isCodeSent, setIsCodeSent] = useState(false);    //flag to check if code is sent
    const [code, setCode] = useState("");   //code from user's input
    const [genCode, setGenCode] = useState(""); //code generagted by program
    const [isCodeMatched, setIsCodeMatched] = useState(true);  //flag to check if user's input is same with generated code
    const [sendSmsMutation] = useMutation(SEND_SMS);
    const { state, dispatch } = useContext(ProfileContext);
    const [sendVerificationEmail, { loading: sendingEmailLoading, error: sendingEmailError }] = useMutation(SEND_VERIFICATION_EMAIL);
    const initialOptions = [
        {
            id: 1,
            title: "Email",
            value: item.email,
            type: 'primary'
        },
        {
            id: 2,
            title: "Phone",
            value: item.phone ? item.phone.number : "No phone yet",
            type: 'secondary'
        }
    ]
    console.log(item)
    const [options, setOptions] = useState(initialOptions);
    const setVerificationOption = (id) => {
        if (id === 2) {
            setOptions(
                [
                    {
                        id: 1,
                        title: "Email",
                        value: item.email,
                        type: 'secondary'
                    },
                    {
                        id: 2,
                        title: "Phone",
                        value: item.phone || 'No phone yet',
                        type: 'primary'
                    }
                ]
            )
        } else {
            setOptions(
                [
                    {
                        id: 1,
                        title: "Email",
                        value: item.email,
                        type: 'primary'
                    },
                    {
                        id: 2,
                        title: "Phone",
                        value: item.phone || 'No phone yet',
                        type: 'secondary'
                    }
                ]
            )
        }
        // op
    }
    const { authState, authDispatch } = useContext<any>(AuthContext);
    const generateEmailCode = () => {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < 10; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    if (sendingEmailError) {
        toast.error('Something went wrong while sending email.', {
            position: "top-center",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined
        });
    }
    const handleSubmit = async (values: FormValues) => {
        const selectedOption = options.find(ele => ele.type === 'primary');
        console.log(selectedOption, item.id, uuid());
        if (selectedOption.id === 1) {
            let email = await sendVerificationEmail(
                { variables: { id: item.id, token: uuid() } }
            );
            toast.success("We've sent a verificaiton email to your email address. Now please check your inbox", {
                position: "top-center",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                onClose: () => closeModal()
            });
        } else {
            const userId = item.id;
            openModal({
                show: true,
                config: {
                    width: 450,
                    height: 'auto',
                    enableResizing: false,
                    disableDragging: true,
                    className: 'add-address-modal',
                },
                closeOnClickOutside: true,
                component: SmsModal,
                componentProps: { item: {id: item.id} }
            });
        }
        // console.log(email)
    };
    const sendCode = async ({ setSubmitting }: any) => {
        authDispatch({
            type: 'TEST'
        });
        // setIsCodeSent(true);
        // const generatedCode = generateCode();
        // let data = await sendSmsMutation({
        //     variables: { phoneNumber: primaryPhone, code: generatedCode },
        // });
        // setGenCode(generatedCode);
        // setIsCodeMatched(true);
    };

    const generateCode = () => {
        const chars = '0123456789'.split('');
        let result = '';
        for (let i = 0; i < 6; i++) {
            var x = Math.floor(Math.random() * chars.length);
            result += chars[x];
        }
        return result;
    }
    return (
        <Formik
            initialValues={initialValues}
            onSubmit={handleSubmit}
        >
            {({
                values,
                handleChange,
                handleBlur,
                isSubmitting,
            }: FormikProps<FormValues>) => (
                    <Form>
                        <Heading>
                            Please verify your account
                        </Heading>
                        <p>You should verify at least one of belows to activate your account.</p>
                        <InformationBox>
                            <VerificationMethod>
                                <RadioGroup
                                    items={options}
                                    component={(item: any) => (
                                        <RadioCard
                                            id={item.id}
                                            key={item.id}
                                            title={item.title}
                                            content={item.value}
                                            name='schedule'
                                            checked={item.type === 'primary'}
                                            withActionButtons={false}
                                            onChange={() =>
                                                setVerificationOption(item.id)
                                            }
                                        />
                                    )}
                                />
                            </VerificationMethod>
                        </InformationBox>
                        <Button
                            onClick={handleSubmit}
                            type='button'
                            // disabled={!isValid}
                            title='Verify'
                            intlButtonId='verifyButtonText'
                            size="small"
                            style={{ float: 'right' }}
                            isLoading={sendingEmailLoading}
                        />
                        <ToastContainer />
                    </Form>
                )}
        </Formik>
    );
};

export default VerificationConfirmModal;

const StyledInput = styled.input`
  width: 100%;
  height: 54px;
  border-radius: 6px;
  font-family: ${themeGet('colors.fontFamily', 'Lato, sans-serif')};
  border: 1px solid ${themeGet('colors.borderColor', '#e6e6e6')};
  color: ${themeGet('colors.darkBold', '#0D1136')};
  font-size: 16px;
  line-height: 19px;
  font-weight: 400;
  padding: 0 18px;
  box-sizing: border-box;
  transition: border-color 0.25s ease;

  &:hover,
  &:focus {
    outline: 0;
  }

  &:focus {
    border-color: ${themeGet('colors.primary', '#009e7f')};
  }

  &::placeholder {
    color: ${themeGet('colorsdarkRegular', '#77798C')};
  }
`;

const StyledError = styled.div`
  color: red;
  padding-bottom: 10px;
  margin-top: -5px;
`;

 const VerificationMethod = styled.div`
  .radioGroup {
    justify-content: space-between;
    > label {
      margin-right: 0;
      flex: calc(50% - 10px);
      max-width: calc(50% - 10px);
      padding: 11px 15px;
      @media (max-width: 900px) and (min-width: 768px) {
        flex: calc(50% - 10px);
        max-width: calc(50% - 10px);
      }

      @media (max-width: 480px) {
        flex: 100%;
        max-width: 100%;
        margin-right: 0;
      }
    }
    span {
        display: block;
        word-break: break-word;        
    }
  }
`;
