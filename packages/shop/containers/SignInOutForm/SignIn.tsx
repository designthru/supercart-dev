import React, { useContext } from 'react';
import {
  LinkButton,
  Button,
  Wrapper,
  Container,
  LogoWrapper,
  Heading,
  SubHeading,
  OfferSection,
  Offer,
  Input,
  Divider,
} from './SignInOutForm.style';
import { Facebook, Google } from 'components/AllSvgIcon';
import { AuthContext } from 'contexts/auth/auth.context';
import { FormattedMessage } from 'react-intl';
import { useMutation } from '@apollo/react-hooks';
import { LOGIN } from 'graphql/mutation/user';
import { closeModal } from '@redq/reuse-modal';
import Image from 'components/Image/Image';
import Supercart from '../../image/Supercart.png';
import { ToastContainer, toast } from 'react-toastify';

export default function SignInModal() {
  const { authDispatch } = useContext<any>(AuthContext);
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [signInMutation, { loading: signInLoading, error: signInError }] = useMutation(LOGIN, {errorPolicy: 'all'});
  
  const toggleSignUpForm = () => {
    authDispatch({
      type: 'SIGNUP',
    });
  };

  const toggleForgotPassForm = () => {
    authDispatch({
      type: 'FORGOTPASS',
    });
  };

  
  const loginCallback = async() => {
    event.preventDefault()
    console.log("clicked...", email, password);
    if (typeof window !== 'undefined') {
      signInMutation({
        variables: {
          email: email, password: password
        }
      }).then( data => 
        {
          console.log(data);
          const address = JSON.parse(data.data.logIn.customer.address);
          const contact = JSON.parse(data.data.logIn.customer.contact);
          const card = JSON.parse(data.data.logIn.customer.card);
          const customer = {
            _id: data.data.logIn.customer._id,
            name: data.data.logIn.customer.name,
            email: data.data.logIn.customer.email,
            image: data.data.logIn.customer.image,
            totalOrder: data.data.logIn.customer.totalOrder,
            totalOrderAmount: data.data.logIn.customer.totalOrderAmount,
            isEmailVerified: data.data.logIn.customer.isEmailVerified,
            isPhoneVerified: data.data.logIn.customer.isPhoneVerified,
            address: address,
            contact: contact,
            card: card
          }
          authDispatch({
            type: 'SIGNIN_SUCCESS',
            payload: { customer: customer, token: data.data.logIn.token}
          });
          closeModal();
        }
      )
      .catch(err => {
          console.log("heerr, ", err.graphQLErrors)
        if (err.graphQLErrors) {
          if (err.graphQLErrors[0].message === 'Password is incorrect') {
            toast.error('The password you input is wrong!', {
              position: "top-center",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined
            });
          } else if (err.graphQLErrors[0].message === 'No customer found') {
            toast.error('No such customer!', {
              position: "top-center",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined
            });
          } else if (err.graphQLErrors[0].message === 'Cannot return null for non-nullable field Mutation.logIn.') {
            toast.error('No customer found!', {
              position: "top-center",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined
            });
          }
        }
        }
      )
      // console.log(signIn);
      // localStorage.setItem('access_token', `${email}.${password}`);
      // authDispatch({ type: 'SIGNIN_SUCCESS' });

      // closeModal();
    }
  };

  return (
    <Wrapper>
      <Container>
        {/* <LogoWrapper>
          <Image url={Supercart} />
        </LogoWrapper> */}

        <Heading>
          <FormattedMessage id='welcomeBack' defaultMessage='Welcome Back' />
        </Heading>

        <SubHeading>
          <FormattedMessage
            id='loginText'
            defaultMessage='Login with your email &amp; password'
          />
        </SubHeading>
        <form onSubmit={loginCallback}>
          <FormattedMessage
            id='emailAddressPlaceholder'
            defaultMessage='Email Address.'
          >
            {(placeholder) => (
              <Input
                type='email'
                placeholder={placeholder}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            )}
          </FormattedMessage>

          <FormattedMessage
            id='passwordPlaceholder'
            defaultMessage='Password (min 6 characters)'
            values={{ minCharacter: 6 }}
          >
            {(placeholder) => (
              <Input
                type='password'
                placeholder={placeholder}
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            )}
          </FormattedMessage>

          <Button
            fullwidth
            title={'Continue'}
            intlButtonId='continueBtn'
            type='submit'
            style={{ color: '#fff' }}
            loading={{signInLoading}}
          />
        </form>
        <Divider>
          <span>
            <FormattedMessage id='orText' defaultMessage='or' />
          </span>
        </Divider>

        <Button
          fullwidth
          title={'Continue with Facebook'}
          className='facebook'
          icon={<Facebook />}
          iconPosition='left'
          iconStyle={{ color: '#ffffff', marginRight: 5 }}
          intlButtonId='continueFacebookBtn'
          onClick={loginCallback}
          style={{ color: '#fff' }}
        />

        <Button
          fullwidth
          title={'Continue with Google'}
          className='google'
          icon={<Google />}
          iconPosition='left'
          iconStyle={{ color: '#ffffff', marginRight: 5 }}
          intlButtonId='continueGoogleBtn'
          onClick={loginCallback}
          style={{ color: '#fff' }}
        />

        <Offer style={{ padding: '20px 0' }}>
          <FormattedMessage
            id='dontHaveAccount'
            defaultMessage="Don't have any account?"
          />{' '}
          <LinkButton onClick={toggleSignUpForm}>
            <FormattedMessage id='signUpBtnText' defaultMessage='Sign Up' />
          </LinkButton>
        </Offer>
      </Container>

      <OfferSection>
        <Offer>
          <FormattedMessage
            id='forgotPasswordText'
            defaultMessage='Forgot your password?'
          />{' '}
          <LinkButton onClick={toggleForgotPassForm}>
            <FormattedMessage id='resetText' defaultMessage='Reset It' />
          </LinkButton>
        </Offer>
      </OfferSection>
      <ToastContainer />
    </Wrapper>
  );
}
