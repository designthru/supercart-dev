import React, { useContext, useState } from 'react';
import Link from 'next/link';
import {
  Button,
  Wrapper,
  Container,
  LogoWrapper,
  Heading,
  SubHeading,
  HelperText,
  Offer,
  Input,
  Divider,
  LinkButton,
} from './SignInOutForm.style';
import { Facebook, Google } from 'components/AllSvgIcon';
import { AuthContext } from 'contexts/auth/auth.context';
import { FormattedMessage } from 'react-intl';
import Image from 'components/Image/Image';
import Supercart from '../../image/Supercart.png';
import { Formik, Form, Field } from "formik";
import { useMutation } from '@apollo/react-hooks';
import { SIGN_UP, SIGN_UP_FROM_CHECKOUT } from 'graphql/mutation/user';
import { ToastProvider, useToasts } from 'react-toast-notifications'
import { ToastContainer, toast } from 'react-toastify';
import { openModal, closeModal } from '@redq/reuse-modal';
import VerificationConfirmModal from './VerificationConfirmModal'
import { ProfileContext } from 'contexts/profile/profile.context';

export default function SignOutModal() {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { authState, authDispatch } = useContext<any>(AuthContext);
  const [signUpMutation, { loading: signUpLoading, error: signUpError }] = useMutation(SIGN_UP_FROM_CHECKOUT);
  const { addToast } = useToasts();
  const { state, dispatch } = useContext(ProfileContext);
  const toggleSignInForm = () => {
    authDispatch({
      type: 'SIGNIN',
    });
  };
  if (signUpError) {
    console.log(signUpError, "rtrrr", signUpError.graphQLErrors, signUpError.networkError);
    if (signUpError.networkError) {
      toast.error(signUpError.networkError.message, {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
    if (signUpError.graphQLErrors[0].message.substring(0, 6) === 'E11000') {
      toast.error('Email already exist!', {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
    return;
  }
  let address, contact, card;
  console.log("current state", state, authState);
  if (!state) {
    address = contact = card = [];
  } else {
    address = state.address;
    card = state.card;
    contact = state.contact;
  }
  let primaryPhone = contact.find(ele => ele.type === 'primary');
  console.log("primary phone number =>", primaryPhone);
  const onSignup = async () => {
    const variable = {
      name: name,
      email: email,
      password: password,
      address: JSON.stringify(address),
      card: JSON.stringify(card),
      contact: JSON.stringify(contact),
    }
    let data = await signUpMutation({
      variables: variable
    });
    console.log("response from server...", data);
    // authDispatch({
      
    //   payload: {
    //     customer: data.data.signUp.customer,
    //     token: data.data.signUp.token
    //   }
    // });
    openModal({
      show: true,
      config: {
        width: 450,
        height: 'auto',
        enableResizing: false,
        disableDragging: true,
        className: 'add-address-modal',
      },
      closeOnClickOutside: true,
      component: VerificationConfirmModal,
      // componentProps: { item: { email: email, phone: primaryPhone, id: "12345" } }
      componentProps: { item: { email: email, phone: primaryPhone, id: data.data.signupFromCheckout.customer._id} },
    });
    // toast.success('Sign up success!', {
    //   position: "top-center",
    //   autoClose: 3000,
    //   hideProgressBar: false,
    //   closeOnClick: true,
    //   pauseOnHover: true,
    //   draggable: true,
    //   progress: undefined,
    //   onClose: () => closeModal()
    // });
  }

  return (

    <Wrapper>
      <Container>
        {/* <LogoWrapper>
          <Image url={Supercart} />
        </LogoWrapper> */}
        <Formik
          initialValues={{
            username: "",
            email: "",
            password: ""
          }}
          onSubmit={onSignup}
        >
          {({ errors, touched }) => (
            <Form>
              <Heading>
                <FormattedMessage id='signUpBtnText' defaultMessage='Sign Up' />
              </Heading>

              <SubHeading>
                <FormattedMessage
                  id='signUpText'
                  defaultMessage='Every fill is required in sign up'
                />
              </SubHeading>

              <FormattedMessage
                id='signupNamePlaceholder'
                defaultMessage='Email Address or Contact No.'
              >
                {(placeholder) => (
                  <Input
                    type='text'
                    placeholder={placeholder}
                    name="username"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    required />
                )
                }
              </FormattedMessage>

              <FormattedMessage
                id='signupEamilAddressPlaceholder'
                defaultMessage='Email Address or Contact No.'
              >
                {(placeholder) =>
                  (
                    <Input
                      type='email'
                      placeholder={placeholder}
                      name="email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      required />
                  )
                }
              </FormattedMessage>

              <FormattedMessage
                id='signupPasswordPlaceholder'
                defaultMessage='Password (min 6 characters)'
              >
                {(placeholder) =>
                  (
                    <Input
                      type='password'
                      placeholder={placeholder}
                      name="password"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      required
                      minLength="6" />
                  )
                }
              </FormattedMessage>

              <HelperText style={{ padding: '20px 0 10px' }}>
                <FormattedMessage
                  id='signUpText'
                  defaultMessage="By signing up, you agree to Supercart's"
                />{' '}
                <Link href='/'>
                  <a>
                    <FormattedMessage
                      id='termsConditionText'
                      defaultMessage='Terms &amp; Condtion'
                    />
                  </a>
                </Link>
              </HelperText>

              <HelperText style={{ padding: '10px 0 30px' }}>
                <Link href='/'>
                  <a>
                    <FormattedMessage
                      id='earnExtraMoneyLink'
                      defaultMessage='Earn extra income become a personal shopper'
                    />
                  </a>
                </Link>
              </HelperText>

              <Button
                fullwidth
                type="submit"
                isLoading={signUpLoading}
                title={'SignUp'}
                intlButtonId='signUpBtn'
                style={{ color: '#fff' }}
              />
              <ToastContainer />
            </Form>
          )}
        </Formik>
        <Divider>
          <span>
            <FormattedMessage id='orText' defaultMessage='or' />
          </span>
        </Divider>

        <Button
          fullwidth
          title={'Continue with Facebook'}
          iconPosition='left'
          className='facebook'
          icon={<Facebook />}
          iconStyle={{ color: '#ffffff', marginRight: 5 }}
          intlButtonId='continueFacebookBtn'
          style={{ color: '#fff' }}
        />

        <Button
          fullwidth
          title={'Continue with Google'}
          className='google'
          iconPosition='left'
          icon={<Google />}
          iconStyle={{ color: '#ffffff', marginRight: 5 }}
          intlButtonId='continueGoogleBtn'
          style={{ color: '#fff' }}
        />
        <Offer style={{ padding: '20px 0' }}>
          <FormattedMessage
            id='alreadyHaveAccount'
            defaultMessage='Already have an account?'
          />{' '}
          <LinkButton onClick={toggleSignInForm}>
            <FormattedMessage id='loginBtnText' defaultMessage='Login' />
          </LinkButton>
        </Offer>
      </Container>
    </Wrapper>
  );
}
