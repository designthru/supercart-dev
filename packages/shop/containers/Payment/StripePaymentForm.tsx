import React, { useContext } from 'react';
import {
  CardElement,
  injectStripe,
  Elements,
  StripeProvider,
  ReactStripeElements,
} from 'react-stripe-elements';
import { useMutation } from '@apollo/react-hooks';
import { closeModal } from '@redq/reuse-modal';
import { GET_PAYMENT } from 'graphql/mutation/order';
import StripeFormWrapper, {
  Heading,
  FieldWrapper,
} from './StripePaymentForm.style';
import { ProfileContext } from 'contexts/profile/profile.context';
import { AuthContext } from 'contexts/auth/auth.context';
import { ADD_CARD } from 'graphql/mutation/user';


type StripeFormProps = ReactStripeElements.InjectedStripeProps & {
  getToken: any;
  buttonText: string;
};
const StripeForm = injectStripe(
  ({ getToken, buttonText, stripe }: StripeFormProps) => {
    const { state, dispatch } = useContext(ProfileContext);
    const { authState, authDispatch } = useContext<any>(AuthContext);
    const [addCardMutation, { loading: addCardLoading, error: addCardError }] = useMutation(ADD_CARD);

    const handleSubmit = async () => {
      if (authState.isAuthenticated) {
        const currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));

        let { token } = await stripe.createToken({ name:  currentCustomer.name});
        getToken(token);
        if (token) {
          console.log(token, 'token');
          authDispatch({
            type: 'ADD_CARD',
            payload: {
              id: token.id,
              name: token.card.name,
              cardType: token.card.brand === 'Visa' ? 'visa' : 'master',
              lastFourDigit: token.card.last4,
              exp_month: token.card.exp_month,
              exp_year: token.card.exp_year
            }
          })
          let addCard = await addCardMutation({
            variables: { userId: currentCustomer._id, cardId: token.id, type: 'secondary', exp_month: token.card.exp_month.toString(), exp_year: token.card.exp_year.toString(), cardType: token.card.brand === 'Visa' ? 'visa' : 'master', lastFourDigit: token.card.last4.toString(), name: currentCustomer.name }
          });
          console.log("response from card mutaion", addCard);
        }
      } else {
        let { token } = await stripe.createToken({ name: "New customer"});
        getToken(token);
        dispatch({ 
          type: 'ADD_CARD',
          payload:  {
            id: token.id,
            name: token.card.name,
            cardType: token.card.brand === 'Visa' ? 'visa' : 'master',
            lastFourDigit: token.card.last4,
            exp_month: token.card.exp_month,
            exp_year: token.card.exp_year
          }});
      }
      closeModal();
    };
    return (
      <StripeFormWrapper>
        <Heading>Enter card info</Heading>
        <FieldWrapper>
          <CardElement />
        </FieldWrapper>
        <button type="button" onClick={handleSubmit}>
          {buttonText ? buttonText : 'Add Card'}
        </button>
      </StripeFormWrapper>
    );
  }
);
type Item = {
  item: {
    price: any;
    buttonText: string;
  };
};
const StripePaymentForm = ({ item: { price, buttonText } }: Item) => {
  const [getPayment] = useMutation(GET_PAYMENT);
  const sendTokenToServer = async (token: any) => {
    const payment_info = await getPayment({
      variables: { paymentInput: JSON.stringify({ token, amount: price }) },
    });
    console.log(payment_info, 'payment_info');
  };

  return (
    process.browser && (
      <StripeProvider apiKey={process.env.STRIPE_PUBLIC_KEY}>
        <div className="example">
          <Elements>
            <StripeForm
              getToken={token => sendTokenToServer(token)}
              buttonText={buttonText}
            />
          </Elements>
        </div>
      </StripeProvider>
    )
  );
};

export default StripePaymentForm;
