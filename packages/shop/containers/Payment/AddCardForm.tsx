import React, {useState, useContext} from 'react';
import { useMutation } from '@apollo/react-hooks';
import CreditCardInput from 'react-credit-card-input';
import { closeModal } from '@redq/reuse-modal';
import { AuthContext } from 'contexts/auth/auth.context';
import { ProfileContext } from 'contexts/profile/profile.context';
import Button from 'components/Button/Button';
import { ADD_CARD } from 'graphql/mutation/user';
import uuid from 'react-uuid';

const AddCardForm = () => {
    const [cvc, setCvc] = useState('');
    const [expiry, setExpiry] = useState('');
    const [name, setName] = useState('');
    const [number, setNumber] = useState('');
    const [error, setError] = useState('');

    const { state, dispatch } = useContext(ProfileContext);
    const { authState, authDispatch } = useContext<any>(AuthContext);

    const [addCardMutation, {loading: addCardLoading, error: addCardError}] = useMutation(ADD_CARD);

    const getCardType = (number) => {
        // visa
        var re = new RegExp("^4");
        if (number.match(re) != null)
            return "visa";

        // Mastercard 
        // Updated for Mastercard 2017 BINs expansion
        if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number))
            return "master";

        // AMEX
        re = new RegExp("^3[47]");
        if (number.match(re) != null)
            return "AMEX";

        // Discover
        re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
        if (number.match(re) != null)
            return "Discover";

        // Diners
        re = new RegExp("^36");
        if (number.match(re) != null)
            return "Diners";

        // Diners - Carte Blanche
        re = new RegExp("^30[0-5]");
        if (number.match(re) != null)
            return "Diners - Carte Blanche";

        // JCB
        re = new RegExp("^35(2[89]|[3-8][0-9])");
        if (number.match(re) != null)
            return "JCB";

        // Visa Electron
        re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
        if (number.match(re) != null)
            return "Visa Electron";

        return "";
    }
    const handleSubmit = async() => {
        console.log("clicked...")
        event.preventDefault();
        if (error) {
            console.log(error);
        } else {
            const realNumber = number.split(" ").join("").trim();
            const type = getCardType(realNumber);
            const currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
            console.log("type...", type);
            const cardId = uuid();
            if (authState.isAuthenticated) {
                authDispatch({
                    type: 'ADD_CARD',
                    payload: {
                        id: cardId,
                        type: 'secondary',
                        cardType: type,
                        name: currentCustomer.name,
                        lastFourDigit: realNumber.substring(realNumber.length - 5, realNumber.length - 1),
                        number: realNumber,
                        expiry: expiry,
                        cvc: cvc
                    }
                })

                let addCard = await addCardMutation({
                    variables: { userId: currentCustomer._id, cardId: cardId, type: 'secondary', number: realNumber, cardType: type, expiry: expiry, cvc: cvc}
                });
                console.log("response from card mutaion", addCard);
            } else {
                dispatch({
                    type: 'ADD_CARD',
                    payload: {
                        id: cardId,
                        brand: type,
                        name: currentCustomer.name,
                        last4: realNumber.substring(realNumber.length - 5, realNumber.length - 1),
                        number: realNumber,
                        expiry: expiry,
                        cvc: cvc
                    }
                })
            }
            closeModal();
        }
    }
    return (
        <div id="PaymentForm">
            <form>
                <CreditCardInput
                    onError={({ inputName, err }) => setError(error)}
                    cardNumberInputProps={{ value: number, onChange: event => setNumber(event.target.value) }}
                    cardExpiryInputProps={{ value: expiry, onChange: event => setExpiry(event.target.value) }}
                    cardCVCInputProps={{ value: cvc, onChange: event => setCvc(event.target.value) }}
                    fieldClassName="input"
                />
                <Button
                    onClick={handleSubmit}
                    type='submit'
                    title='Save Card'
                    size='small'
                    fullwidth={true}
                    style={{marginTop: "10px"}}
                />
            </form>
        </div>
    );
};

export default AddCardForm;