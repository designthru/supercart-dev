const { withPlugins } = require('next-compose-plugins');
const withOptimizedImages = require('next-optimized-images');

// next.js configuration
const nextConfig = {
  env: {
    STRIPE_PUBLIC_KEY: 'pk_test_PRjzYC33osZQSl2PsooUTCKO00xQkt9kca',
    API_URL:
      process.env.NODE_ENV === 'development'
        ? 'http://localhost:4000/supercart/graphql'
        : 'http://18.206.83.109:4000/supercart/graphql',
  },
  webpack: config => {
    config.resolve.modules.push(__dirname);

    config.resolve.alias = {
      ...config.resolve.alias,
    };
    return config;
  },
};

module.exports = withPlugins([withOptimizedImages], nextConfig);
