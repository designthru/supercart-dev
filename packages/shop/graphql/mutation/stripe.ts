import gql from 'graphql-tag';

export const CREATE_CUSTOMER = gql`
  mutation($id: String!, $email: String!, $name: String!, $phone: String!, $source: String!, $coupon: String!, $totalPrice: String!, $totalOrder: String!) {
    createCustomer(id: $id, email: $email, name: $name, phone: $phone, source: $source, coupon: $coupon, totalPrice: $totalPrice, totalOrder: $totalOrder) {
         _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified,
        customerId
    }
  }
`;

export const GET_COUPON = gql`
  mutation($id: String!) {
    getCoupon(id: $id) {
       code,
       name,
       discountInPercent,
       discountInFixed
    }
  }
`;