import gql from 'graphql-tag';

export const SEND_SMS = gql`
  mutation($phoneNumber: String!, $code: String!) {
    sendSmsCode(phoneNumber: $phoneNumber, code: $code) {
      sid,
      body,
      to,
      from,
      status,
    }
  }
`;
