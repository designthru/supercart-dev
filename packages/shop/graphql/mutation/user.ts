import gql from 'graphql-tag';

export const SIGN_UP = gql`
  mutation($username: String!, $email: String!, $password: String!) {
    signUp(username: $username, email: $email, password: $password) {
      customer {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address {
          id,
          type,
          name,
          info
        },
        contact {
          id,
          type,
          number
        },
        card {
          id,
          type,
          name,
          cardType,
          lastFourDigit
        },
        isPhoneVerified,
        isEmailVerified
      },
      token
    }
  }
`;

export const LOGIN = gql`
  mutation($email: String!, $password: String!) {
    logIn(email: $email, password: $password) {
      customer {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
      },
      token
    }
  }
`;

export const GET_ALL_USERS = gql`
  mutation($data: String) {
      getAllUsers(data: $data) {
          _id,
          name,
          email
      }
  }
`;

export const SIGN_UP_FROM_CHECKOUT = gql`
  mutation($name: String!, $email: String!, $password: String!, $card: String!,$address: String!, $contact: String!) {
    signupFromCheckout(name: $name, email: $email, password: $password, card: $card, address: $address, contact: $contact) {
      customer {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
      },
      token
    }
  }
`;

export const SEND_VERIFICATION_EMAIL = gql`
  mutation($id: String!, $token: String!) {
    sendVerificationEmail(id: $id, token: $token) {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }
  }
`;

export const CHECK_EMAIL_TOKEN = gql`
  mutation($token: String!) {
    checkEmailToken(token: $token) {
      customer {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
      },
      token
    }
  }
`;

export const DO_PHONE_VERIFY = gql`
  mutation($id: String!) {
    doPhoneVerify(id: $id) {
      customer {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
      },
      token
    }
  }
`;

export const ADD_ADDRESS = gql`
  mutation($userId: String!, $addressId: String!, $type: String!, $name: String!, $info: String!) {
    addAddress(userId: $userId, addressId: $addressId, type: $type, name: $name, info: $info) {
       _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }    
  }
`;

export const EDIT_ADDRESS = gql`
  mutation($userId: String!, $addressId: String!, $type: String!, $name: String!, $info: String!) {
    editAddress(userId: $userId, addressId: $addressId, type: $type, name: $name, info: $info) {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }
  }
`;

export const CHANGE_PRIMARY_ADDRESS = gql`
  mutation($userId: String!, $addressId: String!) {
    changePrimaryAddress(userId: $userId, addressId: $addressId) {
      _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }
  }
`;

export const DELETE_ADDRESS = gql`
  mutation($userId: String!, $addressId: String!) {
    deleteAddress(userId: $userId, addressId: $addressId) {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }
  }
`;

export const ADD_CONTACT = gql`
  mutation($userId: String!, $contactId: String!, $type: String!, $number: String!) {
    addContact(userId: $userId, contactId: $contactId, type: $type, number: $number) {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }
  }
`;

export const EDIT_CONTACT = gql`
  mutation($userId: String!, $contactId: String!, $type: String!, $number: String!) {
    editContact(userId: $userId, contactId: $contactId, type: $type, number: $number) {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }
  }
`;

export const DELETE_CONTACT = gql`
  mutation($userId: String!, $contactId: String!) {
    deleteContact(userId: $userId, contactId: $contactId) {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }
  }
`;

export const CHANGE_PRIMARY_CONTACT = gql`
  mutation($userId: String!, $contactId: String!) {
    changePrimaryContact(userId: $userId, contactId: $contactId) {
      _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }
  }
`;

export const ADD_CARD = gql`
  mutation($userId: String!, $cardId: String!, $type: String!, $exp_month: String!, $exp_year: String!, $cardType: String!, $lastFourDigit: String!, $name: String!) {
    addCard(userId: $userId, cardId: $cardId, type: $type, exp_month: $exp_month, exp_year: $exp_year, cardType: $cardType, lastFourDigit: $lastFourDigit, name: $name) {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }
  }
`;

export const CHANGE_PRIMARY_CARD = gql`
  mutation($userId: String!, $cardId: String!) {
    changePrimaryCard(userId: $userId, cardId: $cardId) {
      _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }
  }
`;

export const DELETE_CARD = gql`
  mutation($userId: String!, $cardId: String!) {
    deleteCard(userId: $userId, cardId: $cardId) {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }
  }
`;

export const CHANGE_USER_INFO = gql`
  mutation($userId: String!, $name: String!, $email: String!) {
    updateUserInfor(userId: $userId, name: $name, email: $email) {
        _id,
        name,
        email,
        image,
        totalOrder,
        totalOrderAmount,
        address,
        contact,
        card,
        isPhoneVerified,
        isEmailVerified
    }
  }
`;