import gql from 'graphql-tag';

export const GET_STORE = gql`
  query getStore($slug: String!) {
    store(slug: $slug) {
      id
      slug
      name
      categories
      previewUrl
      thumbnailUrl
      type
      deliveryDetails {
        charge
        minimumOrder
        isFree
      }
      address
      products {
        id
        name
        type
        description
        price
      }
    }
  }
`;
