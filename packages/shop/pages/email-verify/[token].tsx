import React, { useContext, useEffect, useState } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import styled from 'styled-components';
import { Modal } from '@redq/reuse-modal';
import { AuthContext } from 'contexts/auth/auth.context';
import { SEO } from 'components/seo';
import SiteFooter from 'components/SiteFooter/SiteFooter';
import { FormattedMessage } from 'react-intl';
import { withApollo } from 'helper/apollo';
import { useRouter } from 'next/router';
import { useMutation } from '@apollo/react-hooks';
import { CHECK_EMAIL_TOKEN } from 'graphql/mutation/user';

type Props = {
    deviceType?: {
        mobile: boolean;
        tablet: boolean;
        desktop: boolean;
    };
};
const ProfilePage: NextPage<Props> = ({ deviceType }) => {
    const { authState, authDispatch } = useContext<any>(AuthContext);
    const [checkEmailTokenMutation, { loading: checkLoading, error: checkError }] = useMutation(CHECK_EMAIL_TOKEN);
    const [isVerified, setIsVerified] = useState(false);
    const [isErorr, setIsError] = useState(false);
    const router = useRouter()
    console.log(router.query);
    const checkEmailToken = async(token) => {
        const checkResult = await checkEmailTokenMutation({
            variables: { token: router.query.token }
        })
        console.log(checkResult);
        const address =JSON.parse(checkResult.data.checkEmailToken.customer.address);
        const contact = JSON.parse(checkResult.data.checkEmailToken.customer.contact);
        const card = JSON.parse(checkResult.data.checkEmailToken.customer.card);
        const customer = {
            _id: checkResult.data.checkEmailToken.customer._id,
            name: checkResult.data.checkEmailToken.customer.name,
            email: checkResult.data.checkEmailToken.customer.email,
            image: checkResult.data.checkEmailToken.customer.image,
            totalOrder: checkResult.data.checkEmailToken.customer.totalOrder,
            totalOrderAmount: checkResult.data.checkEmailToken.customer.totalOrderAmount,
            isEmailVerified: checkResult.data.checkEmailToken.customer.isEmailVerified,
            isPhoneVerified: checkResult.data.checkEmailToken.customer.isPhoneVerified,
            address: address,
            contact: contact,
            card: card
        }
        setIsVerified(true);
        authDispatch({ type: 'SIGNUP_SUCCESS', payload: { customer: customer, token: checkResult.data.checkEmailToken.token}});
        const timer = setTimeout(() => {
            router.push('/profile');
        }, 3000);
        return () => clearTimeout(timer);
    }   
    if (checkError) {
        setIsError(true)
    }
    useEffect(() => {
        checkEmailToken(router.query.token);
    }, [])
    return (
        <Modal>
            <SEO title='Email Verificaiton' description='Email Verification' />
            <HelpPageWrapper>
                <HelpPageContainer>
                    <Heading>Email Verification</Heading>
                    {
                        isVerified && 
                        <div style={{ textAlign: 'center' }}>Your email has been verified successfully. Page will be automatically redirected in 3 seconds...</div>
                    }
                    {
                        isErorr &&
                        <div style={{ textAlign: 'center' }}>Something went wrong while verifying emial.</div>
                    }
                    {
                        checkLoading &&
                        <div style={{ textAlign: 'center' }}>Verifying now...</div>
                    }
                </HelpPageContainer>

                <SiteFooter style={{ marginTop: 50 }}>
                    <FormattedMessage
                        id='siteFooter'
                        defaultMessage='Supercart is a product of'
                    />
          &nbsp; <a href='#'>Redq, Inc.</a>
                </SiteFooter>
            </HelpPageWrapper>
        </Modal>
    );
};

export default withApollo(ProfilePage);

const Heading = styled.h3`
  font-size: 21px;
  font-weight: 700;
  color: #0d1136;
  line-height: 1.2;
  margin-bottom: 25px;
  width: 100%;
  text-align: center;
`;

const HelpPageWrapper = styled.div`
  background-color: #f7f7f7;
  position: relative;
  padding: 130px 0 60px 0;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  @media (max-width: 989px) {
    padding-top: 70px;
  }
`;

export const HelpPageContainer = styled.div`
  background-color: transparent;
  padding: 0;
  border-radius: 6px;
  overflow: hidden;
  position: relative;
  @media (min-width: 990px) {
    width: 870px;
    margin-left: auto;
    margin-right: auto;
  }

  @media (max-width: 989px) {
    padding: 30px;
  }
`;