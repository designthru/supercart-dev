import React, { useContext } from 'react';
import { NextPage } from 'next';
import { useQuery } from '@apollo/react-hooks';
import { Modal } from '@redq/reuse-modal';
import { withApollo } from 'helper/apollo';
import { SEO } from 'components/seo';
import Checkout from 'containers/CheckoutWithSidebar/CheckoutWithSidebar';
import { GET_LOGGED_IN_CUSTOMER } from 'graphql/query/customer.query';
import { AuthContext } from 'contexts/auth/auth.context';
import { ProfileProvider } from 'contexts/profile/profile.provider';

type Props = {
  deviceType: {
    mobile: boolean;
    tablet: boolean;
    desktop: boolean;
  };
};
const CheckoutPage: NextPage<Props> = ({ deviceType }) => {
  const { authState, authDispatch } = useContext<any>(AuthContext);
  const { data, error, loading } = useQuery(GET_LOGGED_IN_CUSTOMER);
  let initData = {};
  if (!authState.isAuthenticated) {
    initData = {
      address: [],
      contact: [],
      card: []
    }
  } else {
    initData = authState.currentCustomer
  }
  if (loading) {
    return <div>loading...</div>;
  }
  if (error) return <div>{error.message}</div>;
  const token = 'true';
  return (
    <>
      <SEO title='Checkout - Supercart' description='Checkout Details' />
      <ProfileProvider initData={initData}>
        <Modal>
          <Checkout token={token} deviceType={deviceType} />
        </Modal>
      </ProfileProvider>
    </>
  );
};

export default withApollo(CheckoutPage);
