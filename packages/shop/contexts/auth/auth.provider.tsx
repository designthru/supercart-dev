import React, { useReducer } from 'react';
import { AuthContext } from './auth.context';
import uuid from 'react-uuid';
const isBrowser = typeof window !== 'undefined';
const INITIAL_STATE = {
  // isAuthenticated: isBrowser && !!localStorage.getItem(''),
  isAuthenticated: isBrowser && !!localStorage.getItem('userToken'),
  currentForm: 'signIn',
  currentCustomer: isBrowser && JSON.parse(localStorage.getItem('currentCustomer'))
};

function reducer(state: any, action: any) {
  console.log(state, 'auth', action.payload);

  switch (action.type) {
    case 'SIGNIN':
      return {
        ...state,
        currentForm: 'signIn',
      };
    case 'SIGNIN_SUCCESS':
      localStorage.setItem('currentCustomer', JSON.stringify(action.payload.customer));
      localStorage.setItem('userToken', action.payload.token);
      return {
        ...state,
        isAuthenticated: true,
        currentCustomer: action.payload.customer
      };
    case 'SIGN_OUT':
      window.localStorage.removeItem('userToken');
      window.localStorage.removeItem('currentCustomer');
      return {
        ...state,
        isAuthenticated: false,
      };
    case 'SIGNUP':
      return {
        ...state,
        currentForm: 'signUp',
      };
    case 'SIGNUP_SUCCESS':
      console.log("signup success co,")
      window.localStorage.setItem('userToken', action.payload.token);
      window.localStorage.setItem('currentCustomer',JSON.stringify(action.payload.customer));
      return {
        ...state,
        currentCustomer: action.payload.customer,
        isAuthenticated: true
      };
    case 'FORGOTPASS':
      return {
        ...state,
        currentForm: 'forgotPass',
      };

    case 'ADD_OR_UPDATE_ADDRESS':
      let currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
      let isExist = currentCustomer.address.find(ele => ele.id === action.payload.id);
      if (isExist) {
        currentCustomer.address = currentCustomer.address.map((item: any) =>
          item.id === action.payload.id ? { ...item, ...action.payload, type: item.type } : item
        );
        window.localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
        return {...state, currentCustomer};
      }
      const newAdress = {
        ...action.payload,
        type: currentCustomer.address.length === 0 ? 'primary' : 'secondary',
      };
      currentCustomer.address.push(newAdress);
      window.localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
      return { ...state, currentCustomer};

    case 'SET_PRIMARY_ADDRESS':
      currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
      currentCustomer.address.forEach(ele => {
        ele.type = 'secondary';
        if (ele.id === action.payload) {
          ele.type = 'primary'
        }
      });
      window.localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
      return {...state, currentCustomer};

    case 'DELETE_ADDRESS':
      currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
      let address = currentCustomer.address.filter(ele => ele.id !== action.payload);
      currentCustomer.address = address;
      window.localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
      return { ...state, currentCustomer };

    case 'ADD_OR_UPDATE_CONTACT':
      currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
      let isContactExist = currentCustomer.contact.find(ele => ele.id === action.payload.id);
      if (isContactExist) {
        currentCustomer.contact = currentCustomer.contact.map((item: any) =>
          item.id === action.payload.id ? { ...item, ...action.payload, type: item.type } : item
        );
        window.localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
        return { ...state, currentCustomer };
      }
      const newContact = {
        ...action.payload,
        type: currentCustomer.contact.length === 0 ? 'primary' : 'secondary',
      };
      currentCustomer.contact.push(newContact);
      localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
      return {...state, currentCustomer}
    
    case 'SET_PRIMARY_CONTACT':
      currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
      currentCustomer.contact.forEach(ele => {
        ele.type = 'secondary';
        if (ele.id === action.payload) {
          ele.type = 'primary'
        }
      });
      window.localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
      return { ...state, currentCustomer }; 

    case 'DELETE_CONTACT':
      currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
      let contact = currentCustomer.contact.filter(ele => ele.id !== action.payload);
      currentCustomer.contact = contact;
      console.log("after changed...", currentCustomer);
      window.localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
      return { ...state, currentCustomer };
    
    case 'ADD_CARD':
      currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
      let isCardExist = currentCustomer.card.find(ele => ele.id === action.payload.id);
      if (isCardExist) {
        currentCustomer.card = currentCustomer.card.map((item: any) =>
          item.id === action.payload.id ? {...item, ...action.payload, type: item.type} : item
        );
        window.localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
        return {...state, currentCustomer}
      }
      const newCard = {
        id: action.payload.id,
        type: currentCustomer.card.length === 0 ? 'primary' : 'secondary',
        cardType: action.payload.cardType.toLowerCase(),
        name: action.payload.name,
        lastFourDigit: action.payload.lastFourDigit,
        exp_month: action.payload.exp_month,
        exp_year: action.payload.exp_year,
      };
      currentCustomer.card.push(newCard);
      localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
      return {
        ...state,
        currentCustomer
      };
    
    case 'DELETE_CARD':
      currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
      let card = currentCustomer.card.filter(ele => ele.id !== action.payload);
      currentCustomer.card = card;
      window.localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
      return { ...state, currentCustomer };
    case 'SET_PRIMARY_CARD':
      currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
      currentCustomer.card.forEach(ele => {
        ele.type = 'secondary';
        if (ele.id === action.payload) {
          ele.type = 'primary'
        }
      });
      window.localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
      return { ...state, currentCustomer };
    
    case 'CHANGE_USER_INFO':
      currentCustomer = JSON.parse(window.localStorage.getItem('currentCustomer'));
      currentCustomer.name = action.payload.name;
      currentCustomer.email = action.payload.email;
      window.localStorage.setItem('currentCustomer', JSON.stringify(currentCustomer));
      return {...state, currentCustomer};
      default:
      return state;
  }
}

export const AuthProvider: React.FunctionComponent = ({ children }) => {
  const [authState, authDispatch] = useReducer(reducer, INITIAL_STATE);
  return (
    <AuthContext.Provider value={{ authState, authDispatch }}>
      {children}
    </AuthContext.Provider>
  );
};
