#!/bin/bash
# Stop all servers and start the server
sudo killall -9 node || true
cd /home/ec2-user/supercart_app
sudo lerna bootstrap
sudo mkdir logs
sudo chmod -R 777 logs/
sudo yarn install
sudo yarn build:prod
sudo nohup yarn prod:api > logs/api.log 2>&1 &
sudo nohup yarn prod:shop > logs/shop.log 2>&1 &
sudo nohup yarn prod:admin > logs/admin.log 2>&1 &
