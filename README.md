# SuperCart

## Overview

Supercart is an on-demand product delivery services that provide shopping and delivery services from a wide arrange of popular local retail stores that include but not limited to grocery stores, hardware stores and big box retailes like HomeDepot, Costos, . User can register as a Buyer. User can become Store (Shop) and Shipper after being approved by admin. Buyer can create a product delivery request. Nearby Shippers will receive that product delivery request, buy products at Store and deliver products to Buyer. Admin will earn commission for each transaction between Buyer vs Shipper. This app template includes various categories of products such as fruits, meat, snacks, beauty, etc. Admin can define each category at the backend.

## Features

This app is for Buyer to make on-demand grocery delivery request. Store (Shop) will provide products on the app, Shipper will deliver the products to Buyer's location. Admin uses web-based backend to manage the users, earn commissions & control the request categories, etc. The app uses Google Map SDK and other Google API Services (Google may charge you for high usage of its API).
Link to Supercart Business Model Diagram on Draw.io

<iframe frameborder="0" style="width:100%;height:700px;" src="https://app.diagrams.net?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=SuperCart.app%20-%20BM%20Diagram.drawio#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D1tJmUafUgtL5IeQS0rsXyAmB5_XTjrS0K%26export%3Ddownload"></iframe>

## User Stories

### Customer/Household:

- Users can register as a Customer, PersonalShopper, or both
- Customer can make a products delivery request
- Nearby Store (Shop) will display for Customer to choose products from
- Nearby Persoanl Shoppers will receive a new shopping task/customer order notification on their mobile phone to shop for and deliver products to the Customer
- Customer can track Personal Shoppers via a map
- Customer make payment via PayPal, Credit Card
- Customer can register as a Personal Shopper on the same app. However, Persoanl Shopper's will be routed to a different app experience
- Customer can rate Product, Shipper, and Stores
- Customers can add a local store
- Customers can add store and product specific coupons at checkout

### Store (Shop):

A store needs to be approved by admin
The store provide the store information with its location on map
The store can add new and manage products
Store handles products to Shipper at the Store. The Store gets payment from Shipper at the store. Then Shipper delivers the products to the Buyer and get paid by the Buyer

### Personal Shopper/Associate:

A Shipper needs to be approved by the admin
Shipper provides his car information and needs to be set online to receive a request
When Shipper receives a product delivery request from a Buyer, Shipper goes to the Seller, pay the products. Then Shipper delivers products to the Buyers and gets paid by the Buyer
The Shipper’s location can be tracked by the Buyer
Shipper can rate the Buyer
Admin:
Admin can manage all Buyers, Stores & Shippers at web-based backend
Admin can define multi categories of Shopping request
Admin can set a commission rate of each transaction between Buyer vs Shipper
Admin can manage Report, Refund and Help requests made by users
